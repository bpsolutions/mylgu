<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

/////////////////////////////DEFAULT CONTROLLER//////////////////////////////////
$route['default_controller'] = 'Login/web_login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
/////////////////////////////////////////////////////////////////////////////////

$route['user-login'] 					= 'Login';
$route['web-login'] 					= 'Login/web_login';
$route['admin-login']['post']			= 'Login/admin_login';

$route['create-establishment']['post'] 	= 'Establishment';
$route['get-establishment-list']['get'] = 'Establishment/get_establishment_list';

$route['add-personnel']['post'] 		= 'Personnel';
$route['get-personnel-list']['get'] 	= 'Personnel/get_personnel_list';

$route['is-vehicle-registered']['get'] 	= 'Border_Control/is_vehicle_registered';
$route['register-vehicle']['post'] 		= 'Border_Control/register_vehicle';
$route['vehicle-destination']['post'] 	= 'Border_Control/vehicle_destination';
$route['scan-passenger-qrcode']['get']  = 'Border_Control/scan_passenger_qrcode';
$route['add-passenger']['post']  		= 'Border_Control/add_passenger';
$route['get-passenger-list']['get']  	= 'Border_Control/get_passenger_list';


//administrator
$route['get-establishment']['get']		            = 'Admin/get_establishment';
$route['update-establishment-status']['post']		= 'Admin/update_establishment_status';

$route['border-control']           					= 'Admin/border_control';
$route['get-border-control']['get']					= 'Admin/get_border_control';
$route['add-border-control']['post']				= 'Admin/add_border_control';
$route['update-border-control']['post']				= 'Admin/update_border_control';

$route['add-bcp']['post']							= 'Admin/add_bcp';
$route['border-control/(:num)']						= 'Admin/view_bcp/$1';
$route['get-bcp']['get']							= 'Admin/get_bcp';

$route['verify-establishment']['post']				= 'Admin/verify_establishment';
$route['denied-establishment']['post']				= 'Admin/denied_establishment';
$route['establishment-log-report/(:num)']			= 'Admin/establishment_log_report/$1';
$route['get-establishment-log']['get']				= 'Admin/get_establishment_log';

$route['generate-qrcode']['post']					= 'Qr_code/generate_qrcode';

$route['web-registration']							= 'Web_Registration';
$route['web-register']['post']						= 'Web_Registration/web_register';

$route['user-list']									= 'User_List';
$route['get-user-list']['get']						= 'User_List/get_user_list';


$route['email-passcode']['post']							= 'Admin/email_passcode';
