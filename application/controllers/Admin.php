<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Admin_Model');
		date_default_timezone_set('Asia/Manila');
		if(!$this->session->userdata('auth')){
			redirect('web-login', 'refresh');
		}

	}

	public function index()
	{
		$this->load->view('admin/_main/head');
		$this->load->view('admin/administrator/establishment/sub_css');
		$this->load->view('admin/_main/body_start');
		$this->load->view('admin/_wrapper/wrapper_start');
		$this->load->view('admin/administrator/establishment/establishment');
		$this->load->view('admin/_wrapper/wrapper_end');
		$this->load->view('admin/_main/main_script');
		$this->load->view('admin/administrator/establishment/sub_js');
		$this->load->view('admin/_main/end_tag');
	}

	public function get_establishment()
	{
		$establishment = $this->Admin_Model->get_establishment();
		$data = array();
		$counter = 0;
		foreach($establishment as $li)
		{
			$row = array();
			$counter++;
			$row['count']  			= $counter;
			$row['id']  			= $li->establishment_id;
			$row['created_by']  	= $li->firstname . ' ' . $li->lastname;
			$row['name']  			= $li->establishment_name;
			$row['address']  		= $li->address;
			$row['permit_number']  	= $li->permit_number;
			$row['str_status']   	= $this->_str_status($li->is_verified);
			$row['status']   		= $li->is_verified;
			$row['date_created']   	= $li->created;
			$data[]     			= $row;
		}
		echo json_encode($data);
	}

	private function _str_status($status){
		if($status === '0') return '<code class="text-warning">Pending</code>';
		else if ($status === '1') return '<code class="text-success">Verified</code>';
		else return '<code class="text-danger">Denied</code>';
	}

	public function verify_establishment()
	{
		$this->form_validation->set_rules('id', 'Establishment', 'trim|required|htmlspecialchars', array('required' => 'Establishment ID not found!'));
		if($this->form_validation->run() === FALSE)
			echo json_encode(array('status' => 400, 'response' => validation_errors()));
		else
		{
			$id 	= $this->input->post('id', TRUE);
			$status = '1';
			$is_exist = $this->Admin_Model->is_establishment_exist($id);
			if ($is_exist)
			{
				$this->Admin_Model->update_establishment_status($id, $status);
				echo json_encode(array('status' => 200, 'response' => 'Establishment status updated!'));
			}
			else
			{
				echo json_encode(array('status' => 404, 'response' => 'Establishment not found'));
			}
		}
	}

	public function denied_establishment()
	{
		$this->form_validation->set_rules('id', 'Establishment', 'trim|required|htmlspecialchars', array('required' => 'Establishment ID not found!'));
		if($this->form_validation->run() === FALSE)
			echo json_encode(array('status' => 400, 'response' => validation_errors()));
		else
		{
			$id 	= $this->input->post('id', TRUE);
			$status = '2';
			$is_exist = $this->Admin_Model->is_establishment_exist($id);
			if ($is_exist)
			{
				$this->Admin_Model->update_establishment_status($id, $status);
				echo json_encode(array('status' => 200, 'response' => 'Establishment status updated!'));
			}
			else
			{
				echo json_encode(array('status' => 404, 'response' => 'Establishment not found'));
			}
		}
	}

	public function establishment_log_report($key){
		$is_exist = $this->Admin_Model->is_establishment_exist($key);
		$data = array();
		if($is_exist){
			$this->session->set_userdata('establishment_id', $key);
			$data['name'] = $this->Admin_Model->get_establishment_name($key);
		}else{
			$data['name'] = "<code>No record found!</code>";
		}

		$this->load->view('admin/_main/head');
		$this->load->view('admin/administrator/establishment/log-report/sub_css');
		$this->load->view('admin/_main/body_start');
		$this->load->view('admin/_wrapper/wrapper_start');
		$this->load->view('admin/administrator/establishment/log-report/log_report', $data);
		$this->load->view('admin/_wrapper/wrapper_end');
		$this->load->view('admin/_main/main_script');
		$this->load->view('admin/administrator/establishment/log-report/sub_js');
		$this->load->view('admin/_main/end_tag');

	}

	public function email_passcode(){
		$permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$pcode = substr(str_shuffle($permitted_chars), 0, 6);

		$email_data = array(
			'logo'				    => 'assets/admin/images/logo/logo-light.png',
			'passcode'          	=> $pcode,
			'email'					=> 'azurekite.l2r@gmail.com'
		);

		$this->load->library('email');
		$this->email->attach($email_data['logo'],'inline');
		$email_data['email_logo'] = $this->email->attachment_cid($email_data['logo']);
		$this->email->from('damonkelens@gmail.com', 'MyLGU');
		$this->email->to($email_data['email']);
		$this->email->subject('Logs Passcode');
		$message = $this->load->view('admin/email/email_passcode.php', $email_data,TRUE);
		$this->email->message($message);
		if ($this->email->send())
		{
			echo json_encode(array('status' => 200, 'pcode' => $pcode, 'response' => 'Please check admin email account for passcode!'));
		}
		else{
			echo json_encode(array('status' => 404, 'response' => 'Failed to send email! Please try again.'));
		}



	}










	public function get_establishment_log()
	{
		$establishment_id = $this->session->establishment_id;
		$log = $this->Admin_Model->get_establishment_log($establishment_id);
		$data = array();
		$counter = 0;
		foreach($log as $li)
		{
			$street 	= $li->street ? $li->street . ' ' : '';
			$barangay 	= $li->barangay ? $li->barangay . ' ' : '';
			$city 		= $li->city ? $li->city . ' ': '';
			$province 	= $li->province ? $li->province . ' ': '';

			$row = array();
			$counter++;
			$row['count']  		= $counter;
			$row['user_id']  	= $li->public_user_id;
			$row['Qr_codeModel']  	= $li->user_qrcode;
			$row['name']  		= $li->firstname . $li->lastname;
			$row['age']  		= $li->age;
			$row['gender']  	= $li->gender;
			$row['contact']  	= $li->contact;
			$row['address']  	= $street . $barangay . $city . $province;
			$row['zip_code']	= $li->zip_code;
			$row['temperature']	= $li->temperature >= 38 ? '<code>'. $li->temperature . '</code>' : $li->temperature;
			$row['date']		= $li->sign_in_date;
			$data[]     		= $row;
		}
		echo json_encode($data);

	}




	//BORDER CONTROL
	public function border_control()
	{
		$data['border_control'] = $this->Admin_Model->get_border_control();
		$this->load->view('admin/_main/head');
		$this->load->view('admin/administrator/border_control/sub_css');
		$this->load->view('admin/_main/body_start');
		$this->load->view('admin/_wrapper/wrapper_start');
		$this->load->view('admin/administrator/border_control/border_control', $data);
		$this->load->view('admin/_wrapper/wrapper_end');
		$this->load->view('admin/_main/main_script');
		$this->load->view('admin/administrator/border_control/sub_js');
		$this->load->view('admin/_main/end_tag');
	}


	public function get_border_control()
	{
		$border_control = $this->Admin_Model->get_border_control();
		$data = array();
		$counter = 0;
		foreach($border_control as $li)
		{
			$bcp_count = $this->Admin_Model->count_bcp($li->bc_id);
			$row = array();
			$counter++;
			$row['count']  			= $counter;
			$row['id']  			= $li->bc_id;
			$row['name']  			= $li->bc_name;
			$row['personnel']  		= "<a href='border-control/" .$li->bc_id. "'>". $bcp_count ."</a>";
			$row['location']  		= $li->bc_location;
			$row['created']  		= $li->created;
			$data[]     			= $row;
		}
		echo json_encode($data);
	}

	public function add_border_control()
	{
		$this->form_validation->set_rules('name', 'Name', 'trim|required|htmlspecialchars|max_length[255]');
		$this->form_validation->set_rules('location', 'Location', 'trim|required|htmlspecialchars|max_length[255]');
		if($this->form_validation->run() === FALSE)
			echo json_encode(array('status' => 400, 'response' => validation_errors()));
		else
		{
			$border_control_data = array(
				'bc_name'		=>	$this->input->post('name', TRUE),
				'bc_location'   => $this->input->post('location', TRUE)
			);
			$this->Admin_Model->insert_border_control($border_control_data);
			echo json_encode(array('status' => 200, 'response' => 'Border control successfully added!'));
		}
	}

	public function update_border_control()
	{
		$this->form_validation->set_rules('id', 'ID', 'trim|required', array('required' => 'ID not found!'));
		$this->form_validation->set_rules('name', 'Name', 'trim|required|htmlspecialchars|max_length[255]');
		$this->form_validation->set_rules('location', 'Location', 'trim|required|htmlspecialchars|max_length[255]');
		if($this->form_validation->run() === FALSE)
			echo json_encode(array('status' => 400, 'response' => validation_errors()));
		else
		{
			$bc_id				=	$this->input->post('id', TRUE);

			$border_control_data = array(
				'bc_name'		=>	$this->input->post('name', TRUE),
				'bc_location'   => $this->input->post('location', TRUE)
			);
			$this->Admin_Model->update_border_control($bc_id, $border_control_data);
			echo json_encode(array('status' => 200, 'response' => 'Border control updated!'));
		}
	}

	public function add_bcp()
	{
		$this->form_validation->set_rules('bc', 'Border Control', 'trim|required');
		$this->form_validation->set_rules('contact', 'Contact', 'trim|required|max_length[20]');
		if($this->form_validation->run() === FALSE)
			echo json_encode(array('status' => 400, 'response' => validation_errors()));
		else
		{
			$contact = $this->input->post('contact', TRUE);
			$is_exist = $this->Admin_Model->is_user_exist($contact);
			if($is_exist){
				$no_role = $this->Admin_Model->user_have_no_role($contact);
				if($no_role){
					$bcp_data = array(
						'bc_id'				=>	$this->input->post('bc', TRUE),
						'bcp_contact'		=>	$contact,
						'bcp_status'		=>	'1',
					);
					$this->Admin_Model->insert_bcp($bcp_data);
					$this->Admin_Model->update_user_type($contact);
					echo json_encode(array('status' => 200, 'response' => 'Border control successfully added!'));
				}
				else{
					echo json_encode(array('status' => 409, 'response' => 'User cannot be set as a personnel. Conflict in user type found!'));
				}
			}
			else {
				echo json_encode(array('status' => 404, 'response' => 'Contact not found!'));
			}

		}
	}

	public function view_bcp($key)
	{
		$this->session->set_userdata('bc_key', $key);
		$this->load->view('admin/_main/head');
		$this->load->view('admin/administrator/border_control/bcp/sub_css');
		$this->load->view('admin/_main/body_start');
		$this->load->view('admin/_wrapper/wrapper_start');
		$this->load->view('admin/administrator/border_control/bcp/bcp');
		$this->load->view('admin/_wrapper/wrapper_end');
		$this->load->view('admin/_main/main_script');
		$this->load->view('admin/administrator/border_control/bcp/sub_js');
		$this->load->view('admin/_main/end_tag');


	}

	public function get_bcp()
	{
		$bc_id = $this->session->bc_key;
		$bcp = $this->Admin_Model->get_bcp($bc_id);
		$data = array();
		$counter = 0;
		foreach($bcp as $li)
		{
			$row = array();
			$counter++;
			$row['count']  			= $counter;
			$row['bcp_id']  		= $li->bcp_id;
			$row['user_id']  		= $li->user_id;
			$row['firstname']  		= $li->firstname;
			$row['lastname']  		= $li->lastname;
			$row['contact']  		= $li->contact;
			$data[]     			= $row;
		}
		echo json_encode($data);

	}

}
?>
