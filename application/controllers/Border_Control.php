<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
class Border_Control extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Border_Control_Model");
	}

	public function is_vehicle_registered()
	{
		$vehicle_qrcode = $this->input->get('vehicle_qrcode', TRUE);
		$is_exist = $this->Border_Control_Model->is_vehicle_exist($vehicle_qrcode);
		if($is_exist){
			echo json_encode(array('status' => 200, 'response' => 'Vehicle found!'));
		}else{
			echo json_encode(array('status' => 404, 'response' => 'Vehicle not found!'));
		}
	}




	public function register_vehicle()
	{
		$this->form_validation->set_rules('vehicle_type', 		'Vehicle Type', 		'trim|required');
		$this->form_validation->set_rules('brand', 				'Brand', 				'trim|required|htmlspecialchars|max_length[50]');
		$this->form_validation->set_rules('color', 				'Color', 				'trim|required|htmlspecialchars|max_length[50]');
		$this->form_validation->set_rules('model', 				'Model', 				'trim|required|htmlspecialchars|max_length[50]');
		$this->form_validation->set_rules('plate_num', 			'Plate Number', 		'trim|required|htmlspecialchars|max_length[20]');
		if($this->form_validation->run() === FALSE)
			echo json_encode(array('status' => 400, 'response' => validation_errors()));
		else
		{
			$vehicle_data = array(
				'vehicle_type'      => $this->input->post('vehicle_type', TRUE),
				'vehicle_qrcode'	=> $this->input->post('vehicle_qrcode', TRUE),
				'brand'         	=> $this->input->post('brand', TRUE),
				'color'         	=> $this->input->post('color', TRUE),
				'model'           	=> $this->input->post('model', TRUE),
				'plate_num'			=> $this->input->post('plate_num', TRUE),
				'is_public'         => $this->input->post('is_public', TRUE),
				'vehicle_img'       => '',
			);
			$vehicle_id = $this->Border_Control_Model->insert_vehicle($vehicle_data);
			echo json_encode(array('status' => 200, 'vehicle_id' => $vehicle_id, 'response' => 'Successfully added vehicle!'));

		}
	}

	public function vehicle_destination()
	{
		$this->form_validation->set_rules('vehicle_id', 		'Vehicle', 			'trim|required|htmlspecialchars', array('required' => 'Vehicle not found!'));
		$this->form_validation->set_rules('from_barangay', 		'Barangay', 		'trim|required|htmlspecialchars|max_length[50]');
		$this->form_validation->set_rules('from_city', 			'City', 			'trim|required|htmlspecialchars|max_length[50]');
		$this->form_validation->set_rules('from_province', 		'Province', 		'trim|required|htmlspecialchars|max_length[50]');
		$this->form_validation->set_rules('from_zip_code', 		'Zip Code', 		'trim|required|htmlspecialchars|max_length[50]');
		$this->form_validation->set_rules('to_barangay', 		'Barangay', 		'trim|required|htmlspecialchars|max_length[50]');
		$this->form_validation->set_rules('to_city', 			'City', 			'trim|required|htmlspecialchars|max_length[50]');
		$this->form_validation->set_rules('to_province', 		'Province', 		'trim|required|htmlspecialchars|max_length[50]');
		$this->form_validation->set_rules('to_zip_code', 		'Zip Code', 		'trim|required|htmlspecialchars|max_length[50]');

		if($this->form_validation->run() === FALSE)
			echo json_encode(array('status' => 400, 'response' => validation_errors()));
		else
		{
			$vehicle_id = $this->input->post('vehicle_id', TRUE);
			$is_exist = $this->Border_Control_Model->is_vehicle_id_exist($vehicle_id);
			if($is_exist){
				$destination_data = array(
					'vehicle_id'      	=> $this->input->post('vehicle_id', TRUE),
					'from_street'		=> $this->input->post('from_street', TRUE),
					'from_barangay'     => $this->input->post('from_barangay', TRUE),
					'from_city'         => $this->input->post('from_city', TRUE),
					'from_province'     => $this->input->post('from_province', TRUE),
					'from_zip_code'		=> $this->input->post('from_zip_code', TRUE),
					'to_street'         => $this->input->post('to_street', TRUE),
					'to_barangay'       => $this->input->post('to_barangay', TRUE),
					'to_city'           => $this->input->post('to_city', TRUE),
					'to_province'		=> $this->input->post('to_province', TRUE),
					'to_zip_code'       => $this->input->post('to_zip_code', TRUE)
				);

				$destination_id = $this->Border_Control_Model->insert_destination($destination_data);
				echo json_encode(array('status' => 200, 'destination_id' => $destination_id, 'response' => 'Successfully added destination!'));

			}else{
				echo json_encode(array('status' => 404, 'response' => 'Vehicle not found!'));
			}
		}
	}


	public function scan_passenger_qrcode()
	{
		$passenger_qrcode = $this->input->get('passenger_qrcode', TRUE);
		$is_exist = $this->Border_Control_Model->is_passenger_exist($passenger_qrcode);
		if($is_exist){
			$user_id = $this->Border_Control_Model->get_passenger_user_id($passenger_qrcode);
			echo json_encode(array('status' => 200, 'response' => $user_id));
		}else{
			echo json_encode(array('status' => 404, 'response' => 'No data found!'));
		}
	}



	public function add_passenger()
	{
		$this->form_validation->set_rules('destination_id', 	'Destination', 		'trim|required|htmlspecialchars', array('required' => 'Destination not set!'));
		$this->form_validation->set_rules('user_id', 			'User', 			'trim|required|htmlspecialchars', array('required' => 'User not found!a'));
		$this->form_validation->set_rules('temperature', 		'Temperature', 		'trim|required|htmlspecialchars|max_length[20]');
		if($this->form_validation->run() === FALSE)
			echo json_encode(array('status' => 400, 'response' => validation_errors()));
		else
		{
			$destination_id = $this->input->post('destination_id', TRUE);
			$is_exist = $this->Border_Control_Model->is_destination_exist($destination_id);
			if($is_exist){
				$user_id = $this->input->post('user_id', TRUE);
				$is_user_exist = $this->Border_Control_Model->is_user_exist($user_id);
				if($is_user_exist){
					$passenger_data = array(
						'destination_id'    => $destination_id,
						'user_id'			=> $this->input->post('user_id', TRUE),
						'temperature'     	=> $this->input->post('temperature', TRUE),
					);
					$passenger_id = $this->Border_Control_Model->insert_passenger($passenger_data);
					echo json_encode(array('status' => 200, 'passenger_id' => $passenger_id, 'response' => 'Passenger successfully added!'));
				} else{
					echo json_encode(array('status' => 404, 'response' => 'User not verified or not found!'));
				}
			}else{
				echo json_encode(array('status' => 404, 'response' => 'Destination not found!'));
			}
		}
	}


	public function get_passenger_list()
	{
		$destination_id = $this->input->get('destination_id', TRUE);
		$is_exist = $this->Border_Control_Model->is_destination_exist($destination_id);
		if($is_exist){
			$passenger_list = $this->Border_Control_Model->get_passenger_list($destination_id);
			echo json_encode(array('status' => 200, 'response' => $passenger_list));
		}else{
			echo json_encode(array('status' => 404, 'response' => 'User not found!'));
		}
	}




}
