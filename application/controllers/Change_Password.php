<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Change_Password extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Change_Password_Model');
		if(!$this->session->userdata('auth')){
			redirect('login', 'refresh');
		}
	}

	public function index()
	{
		$this->load->view('public/_main/main_head');
		$this->load->view('public/change_pass/sub_css');
		$this->load->view('public/_main/body_start');
		$this->load->view('public/_wrapper/wrapper_start');

		$this->load->view('public/change_pass/change_pass');

		$this->load->view('public/_wrapper/wrapper_end');
		$this->load->view('public/_main/main_script');
		$this->load->view('public/change_pass/sub_script');
		$this->load->view('public/_main/end_tag');
	}

	public function change_user_pass()
	{
		$this->form_validation->set_rules('old_pass', 'Old Password', 'trim|required');
		$this->form_validation->set_rules('new_pass', 'New Password', 'trim|required|min_length[8]');
		$this->form_validation->set_rules('conf_new_pass', 'Password Confirmation', 'trim|required|matches[new_pass]');
		if($this->form_validation->run() === FALSE)
			echo json_encode(array('res' => validation_errors(), 'stat' => false));
		else
		{
			$old_pass = $this->Change_Password_Model->get_user_old_password($this->session->auth['user_id']);
			$input_old_pass = md5($this->input->post('old_pass',TRUE));
			if($input_old_pass == $old_pass)
			{
				$new_pass = md5($this->input->post('new_pass',TRUE));
				$this->Change_Password_Model->update_user_password($this->session->auth['user_id'], $new_pass);
				echo json_encode(array('res' => 'Password updated', 'stat' => true));
			}
			else
			{
				echo json_encode(array('res' => 'Invalid old password!', 'stat' => false));
			}
		}
	}
}
?>
