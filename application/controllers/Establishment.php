<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
class Establishment extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Establishment_Model");
	}

	public function index()
	{
		$this->form_validation->set_rules('user_id', 			'User ID', 				'trim|required|htmlspecialchars', array('required' => 'User ID not found!'));
		$this->form_validation->set_rules('establishment_name', 'Establishment Name', 	'trim|required|htmlspecialchars|max_length[255]');
		$this->form_validation->set_rules('address', 			'Address', 				'trim|required|htmlspecialchars|max_length[255]');
		$this->form_validation->set_rules('permit_number', 		'Permit Number', 		'trim|required|htmlspecialchars|max_length[60]');
		if($this->form_validation->run() === FALSE)
			echo json_encode(array('status' => 400, 'response' => validation_errors()));
		else
		{
			$user_id = $this->input->post('user_id', TRUE);
			$is_exist = $this->Establishment_Model->is_user_exist($user_id);
			if($is_exist){
				$is_verified = $this->Establishment_Model->is_verified_user($user_id);
				if($is_verified){
					$establishment_data = array(
						'user_id'           	=> $user_id,
						'establishment_name'	=> $this->input->post('establishment_name', TRUE),
						'address'         		=> $this->input->post('address', TRUE),
						'permit_number'         => $this->input->post('permit_number', TRUE),
					);
					$this->Establishment_Model->insert_establishment($establishment_data);
					echo json_encode(array('status' => 200, 'response' => 'Successfully added establishment!'));

				}else{
					echo json_encode(array('status' => 401, 'response' => 'Mobile number is not verified!'));
				}
			}else{
				echo json_encode(array('status' => 404, 'response' => 'User not found!'));
			}
		}
	}


	public function get_establishment_list()
	{
		$user_id = $this->input->get('user_id', TRUE);
		$is_exist = $this->Establishment_Model->is_user_exist($user_id);
		if($is_exist){
			$is_verified = $this->Establishment_Model->is_verified_user($user_id);
			if($is_verified){
				$have_establishment = $this->Establishment_Model->have_establishment($user_id);
				if($have_establishment){
					$establishment_list = $this->Establishment_Model->get_establishment_list($user_id);
					echo json_encode(array('status' => 200, 'response' => $establishment_list));
				}else{
					echo json_encode(array('status' => 404, 'response' => 'No establishment found!'));
				}
			}else{
				echo json_encode(array('status' => 401, 'response' => 'Mobile number is not verified!'));
			}
		}else{
			echo json_encode(array('status' => 404, 'response' => 'User not found!'));
		}
	}


}
