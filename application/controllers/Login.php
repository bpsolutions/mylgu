<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//header('Access-Control-Allow-Origin: *');
//header('Content-Type: application/json');

class Login extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Login_Model");
	}

	public function index()
	{
		$this->form_validation->set_rules('contact',	'Contact', 	'trim|required|htmlspecialchars|max_length[20]');
		$this->form_validation->set_rules('password', 	'Password', 'trim|required');
		if($this->form_validation->run() === FALSE)
			echo json_encode(array('status' => 400, 'response' => validation_errors()));
		else
		{
			$login_contact 	= $this->input->post('contact', TRUE);
			$login_password = $this->input->post('password', TRUE);

			$is_exist = $this->Login_Model->is_user_exist($login_contact);
			if($is_exist){
				$user_pass = $this->Login_Model->get_user_password($login_contact);
				$is_match = password_verify($login_password, $user_pass);
				if($is_match){
					$user_data = $this->Login_Model->get_user_data($login_contact);
					$data = array(
						'user_id' 				=> $user_data->user_id,
						'user_qrcode' 			=> $user_data->user_qrcode,
						'user_type' 			=> $user_data->user_type,
						'firstname'             => $user_data->firstname,
						'middlename'         	=> $user_data->middlename,
						'lastname'          	=> $user_data->lastname,
						'age '         			=> $user_data->age,
						'gender'      			=> $user_data->gender,
						'contact'               => $user_data->contact,
						'street'               	=> $user_data->street,
						'barangay'              => $user_data->barangay,
						'city'               	=> $user_data->city,
						'province'              => $user_data->province,
						'zip_code'              => $user_data->zip_code,
					);
					echo json_encode(array('status' => 200, 'response' => $data));
				}
				else{
					echo json_encode(array('status' => 401, 'response' => 'Incorrect password!'));
				}
			}
			else{
				echo json_encode(array('status' => 404, 'response' => 'Not found!'));
			}
		}
	}


	public function web_login()
	{
		$this->load->view('public/_main/main_head');
		$this->load->view('public/login/sub_css');
		$this->load->view('public/_main/body_start');
		$this->load->view('public/_wrapper/wrapper_start');

		$this->load->view('public/login/login');

		$this->load->view('public/_wrapper/wrapper_end');
		$this->load->view('public/_main/main_script');
		$this->load->view('public/login/sub_script');
		$this->load->view('public/_main/end_tag');

	}

	public function admin_login()
	{
		$this->form_validation->set_rules('contact', 'Contact', 'trim|required|htmlspecialchars|max_length[20]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		if($this->form_validation->run() === FALSE)
			echo json_encode(array('res' => validation_errors(), 'stat' => false));
		else
		{
			$login_contact 	= $this->input->post('contact', TRUE);
			$login_password = $this->input->post('password', TRUE);

			$is_exist = $this->Login_Model->is_user_exist($login_contact);
			if($is_exist){
				$is_verified = $this->Login_Model->get_user_status($login_contact);
				if($is_verified){
					$user_pass = $this->Login_Model->get_user_password($login_contact);
					$is_match = password_verify($login_password, $user_pass);
					if($is_match){
						$user_type = $this->Login_Model->get_user_type($login_contact);
						if($user_type === '4'){
							$user_data = $this->Login_Model->get_user_data($login_contact);
							$this->session->set_userdata('auth', array(
									'user_id' 		=> $user_data->user_id,
									'user_qrcode' 	=> $user_data->user_qrcode,
									'user_type' 	=> $user_data->user_type,
									'firstname'     => $user_data->firstname,
									'middlename'    => $user_data->middlename,
									'lastname'      => $user_data->lastname,
									'sessionid' 	=> $this->session->userdata('session_id'))
							);
							echo json_encode(array('status' => 200, 'response' => 'Successfully login!'));
						}
						else{
							echo json_encode(array('status' => 401, 'response' => 'Permission denied!'));
						}
					}
					else{
						echo json_encode(array('status' => 401, 'response' => 'Incorrect password!'));
					}
				}
				else{
					echo json_encode(array('status' => 404, 'response' => 'Not found!'));
				}

			}else{
					echo json_encode(array('status' => 401, 'response' => 'Mobile number is not verified!'));
				}
		}
	}

}
