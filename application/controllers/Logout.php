<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Logout extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if(!$this->session->userdata('auth')){
			redirect(base_url(), 'refresh');
		}
	}

	public function index()
	{
		$this->session->unset_userdata('auth');
		$this->session->sess_destroy();
		redirect(base_url(), 'refresh');
	}
}
?>
