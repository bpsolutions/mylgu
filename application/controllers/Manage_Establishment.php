<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Manage_Establishment extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Establishment_Model');
		date_default_timezone_set('Asia/Manila');
		if (!$this->session->userdata('auth')) {
			redirect('web-login', 'refresh');
		}
	}

	public function index()
	{
		$data['category'] = $this->Product_Model->get_category_list();
		$this->load->view('admin/_main/head');
		$this->load->view('admin/staff/product/manage-product/sub_css');
		$this->load->view('admin/_main/body_start');
		$this->load->view('admin/_wrapper/wrapper_start');
		$this->load->view('admin/staff/product/manage-product/manage_product', $data);
		$this->load->view('admin/_wrapper/wrapper_end');
		$this->load->view('admin/_main/main_script');
		$this->load->view('admin/staff/product/manage-product/sub_js');
		$this->load->view('admin/_main/end_tag');
	}

	public function get_establishment()
	{
		$product = $this->Product_Model->get_product();
		$data = array();
		$no = $_POST['start'];
		foreach ($product as $li) {
			$no++;
			$row = array();
//			$row[] = $no;
			$row[] = $li->p_code;
			$row[] = $li->p_name;
			$row[] = $li->category_name;
			if ($li->p_quantity <= 3) {
				$row[] = '<code class="text-warning font-14 font-medium">' . $li->p_quantity . '</code>';
			} else {
				$row[] = '<code class="text-info font-14 font-medium">' . $li->p_quantity . '</code>';
			}

			$row[] = $li->p_brand;
			$row[] = $li->p_price;
			if ($li->p_status == '0')
				$row[] = '<span class="label label-danger">Off</span>';
			else
				$row[] = '<span class="label label-success">On</span>';

			if ($li->is_featured == '0')
				$row[] = '<span class="label label-danger">No</span>';
			else
				$row[] = '<span class="label label-success">Yes</span>';

			$row[] = '<button class="btn btn-sm btn-secondary edit-product"  data-toggle="modal" data-target="#edit-product-modal" data-pid="' . $li->product_id . '"  title="Edit"><i class="fa fa-edit"></i></button>';
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->Product_Model->count_all(),
			"recordsFiltered" => $this->Product_Model->count_filtered(),
			"data" => $data
		);
		echo json_encode($output);
	}
}
