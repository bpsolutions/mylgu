<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
class Personnel extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Personnel_Model");
	}

	public function index()
	{
		$this->form_validation->set_rules('contact', 			'Mobile Number', 		'trim|required|htmlspecialchars|max_length[20]', array('required' => 'User not found!'));
		$this->form_validation->set_rules('establishment_id', 	'Establishment', 		'trim|required|htmlspecialchars', array('required' => 'Establishment not found!'));
		if($this->form_validation->run() === FALSE)
			echo json_encode(array('status' => 400, 'response' => validation_errors()));
		else
		{
			$contact = $this->input->post('contact', TRUE);
			$is_exist = $this->Personnel_Model->is_user_exist($contact);
			if($is_exist){
				$is_verified = $this->Personnel_Model->is_verified_user($contact);
				if($is_verified){
					$establishment_id = $this->input->post('establishment_id', TRUE);
					$is_verified_establishment = $this->Personnel_Model->is_verified_establishment($establishment_id);
					if($is_verified_establishment){
						$personnel_data = array(
							'contact'           => $contact,
							'establishment_id'	=> $this->input->post('establishment_id', TRUE),
						);
						$this->Personnel_Model->insert_personnel($personnel_data);
						echo json_encode(array('status' => 200, 'response' => 'Successfully added personnel!'));
					}
					else{
						echo json_encode(array('status' => 401, 'response' => 'Establishment is not verified!'));
					}
				}else{
					echo json_encode(array('status' => 401, 'response' => 'Mobile number is not verified!'));
				}
			}else{
				echo json_encode(array('status' => 404, 'response' => 'User not found!'));
			}
		}
	}

	public function get_personnel_list()
	{
		$establishment_id = $this->input->get('establishment_id', TRUE);
		$is_exist = $this->Personnel_Model->is_establishment_exist($establishment_id);
		if($is_exist){
			$have_personnel = $this->Personnel_Model->have_personnel($establishment_id);
			if($have_personnel){
				$personnel_list = $this->Personnel_Model->get_personnel_list($establishment_id);
				echo json_encode(array('status' => 200, 'response' => $personnel_list));
			}else{
				echo json_encode(array('status' => 404, 'response' => 'No personnel found!'));
			}

		}else{
			echo json_encode(array('status' => 404, 'response' => 'Establishment not found!'));
		}
	}


}
