<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Profile extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Profile_Model');
		if(!$this->session->userdata('auth')){
			redirect('login', 'refresh');
		}
	}

	public function index()
	{
		$data['user_profile'] = $this->Profile_Model->get_user_profile($this->session->auth['user_id']);

		$this->load->view('public/_main/main_head');
		$this->load->view('public/profile/sub_css');
		$this->load->view('public/_main/body_start');
		$this->load->view('public/_wrapper/wrapper_start');

		$this->load->view('public/profile/profile', $data);

		$this->load->view('public/_wrapper/wrapper_end');
		$this->load->view('public/_main/main_script');
		$this->load->view('public/profile/sub_script');
		$this->load->view('public/_main/end_tag');
	}

	public function update_user_profile()
	{
		$this->form_validation->set_rules('fname', 'Firstname', 'trim|required|htmlspecialchars');
		$this->form_validation->set_rules('lname', 'Lastname', 'trim|required|htmlspecialchars');
		$this->form_validation->set_rules('age', 'Age', 'trim|required|htmlspecialchars');
		$this->form_validation->set_rules('gender', 'gender', 'trim|required|htmlspecialchars');
		$this->form_validation->set_rules('contact', 'Contact', 'trim|htmlspecialchars');
		if($this->form_validation->run() === FALSE)
			echo json_encode(array('res' => validation_errors(), 'stat' => false));
		else
		{
			$user_profile = array(
				'firstname'             => $this->input->post('fname', TRUE),
				'middlename'         	=> $this->input->post('mname', TRUE),
				'lastname'          	=> $this->input->post('lname', TRUE),
				'gender'      			=> $this->input->post('gender', TRUE),
				'age '         			=> $this->input->post('age', TRUE),
				'address'  				=> $this->input->post('address', TRUE),
				'contact'               => $this->input->post('contact', TRUE),
			);
			$this->Profile_Model->update_user_profile($this->session->auth['user_id'],$user_profile);
			echo json_encode(array('res' => 'Successfully updated', 'stat' => true));
		}
	}
	
	public function get_my_medical_record()
	{
		$mr_data	= $this->Profile_Model->get_medical_record($this->session->auth['user_id']);
		$output 	= '';
		if (!empty($mr_data))
		{
		    $gender 	= ''; if($mr_data->gender = 'M'){$gender = 'Male';} else{$gender = 'Female';}
			$dr_data	= $this->Profile_Model->get_doctor_info($mr_data->doctor_id);
			$output .= '<div class="row">
						<div class="col-md-12">
									<form class="form-horizontal" role="form">
										<div class="form-body">
										<h5 class="box-title">Personal Information</h5>
											<hr class="m-t-0 m-b-20">
											<div class="row">
												<div class="col-md-6">
													<div class="row">
														<label class="control-label text-right col-md-4">Firstname:</label>
														<div class="col-md-8">
															<p class="form-control-static"><code class="text-inverse font-14 font-bold">'.$mr_data->firstname.'</code></p>
														</div>
													</div>
												</div>
												<!--/span-->
												<div class="col-md-6">
													<div class="row">
														<label class="control-label text-right col-md-4">Lastname:</label>
														<div class="col-md-8">
															<p class="form-control-static"><code class="text-inverse font-14 font-bold">'.$mr_data->lastname.'</code></p>
														</div>
													</div>
												</div>
												<!--/span-->
											</div>
											<!--/row-->
											<div class="row">
												<div class="col-md-6">
													<div class="row">
														<label class="control-label text-right col-md-4">Gender:</label>
														<div class="col-md-8">
															<p class="form-control-static"><code class="text-inverse font-14 font-bold">'.$gender.'</code></p>
														</div>
													</div>
												</div>
												<!--/span-->
												<div class="col-md-6">
													<div class="row">
														<label class="control-label text-right col-md-4">Age:</label>
														<div class="col-md-8">
															<p class="form-control-static"><code class="text-inverse font-14 font-bold">'.$mr_data->age.'</code></p>
														</div>
													</div>
												</div>
												<!--/span-->
											</div>
											<!--/row-->
											<div class="row">
												<div class="col-md-6">
													<div class="row">
														<label class="control-label text-right col-md-4">Contact:</label>
														<div class="col-md-8">
															<p class="form-control-static"><code class="text-inverse font-14 font-bold">'.$mr_data->contact.'</code></p>
														</div>
													</div>
												</div>
												<!--/span-->
												<div class="col-md-6">
													<div class="row">
														<label class="control-label text-right col-md-4">Address:</label>
														<div class="col-md-8">
															<p class="form-control-static"><code class="text-inverse font-14 font-bold">'.$mr_data->address.'</code></p>
														</div>
													</div>
												</div>
												<!--/span-->
											</div>
											<!--/row-->
											<br>
											<h5 class="box-title m-t-20">Eye Details</h5>
											<hr class="m-t-0 m-b-20">
											<div class="table-responsive" style="clear: both; padding: 5px 20px;">
												<table class="table table-hover" id="a-invoice-table">
													<thead>
													<tr>
														<th style="font-size: 16px;">Eye</th>
														<th style="font-size: 16px;">Sphere</th>
														<th style="font-size: 16px;">Cylinder</th>
														<th style="font-size: 16px;">Axis</th>
														<th style="font-size: 16px;">VD</th>
														<th style="font-size: 16px;">Near Add</th>
														<th style="font-size: 16px;">VN</th>
													</tr>
													</thead>
													<tbody>
													<tr>
														<td><p>OD (Right)</p></td>
														<td><p><code class="text-inverse font-14 font-bold">'.$mr_data->od_sphere.'</code></p></td>
														<td><p><code class="text-inverse font-14 font-bold">'.$mr_data->od_cylinder.'</code></p></td>
														<td><p><code class="text-inverse font-14 font-bold">'.$mr_data->od_axis.'</code></p></td>
														<td><p><code class="text-inverse font-14 font-bold">'.$mr_data->od_vd.'</code></p></td>
														<td><p><code class="text-inverse font-14 font-bold">'.$mr_data->od_near_add.'</code></p></td>
														<td><p><code class="text-inverse font-14 font-bold">'.$mr_data->od_vn.'</code></p></td>
													</tr>
													<tr>
														<td><p>OS (Left)</p></td>
														<td><p><code class="text-inverse font-14 font-bold">'.$mr_data->os_sphere.'</code></p></td>
														<td><p><code class="text-inverse font-14 font-bold">'.$mr_data->os_cylinder.'</code></p></td>
														<td><p><code class="text-inverse font-14 font-bold">'.$mr_data->os_axis.'</code></p></td>
														<td><p><code class="text-inverse font-14 font-bold">'.$mr_data->os_vd.'</code></p></td>
														<td><p><code class="text-inverse font-14 font-bold">'.$mr_data->os_near_add.'</code></p></td>
														<td><p><code class="text-inverse font-14 font-bold">'.$mr_data->os_vn.'</code></p></td>
													</tr>
													</tbody>
												</table>
											</div>

											<h5 class="box-title m-t-20">Lens</h5>
											<hr class="m-t-0 m-b-20">
											<div class="row">
												<div class="col-md-6">
													<div class="row">
														<label class="control-label text-right col-md-4">Lens Type:</label>
														<div class="col-md-8">
															<p class="form-control-static"><code class="text-inverse font-14 font-bold">'.$mr_data->lens_type.'</code></p>
														</div>
													</div>
												</div>
												<!--/span-->
												<div class="col-md-6">
													<div class="row">
														<label class="control-label text-right col-md-4">Lens For:</label>
														<div class="col-md-8">
															<p class="form-control-static"><code class="text-inverse font-14 font-bold">'.$mr_data->lens_for.'</code></p>
														</div>
													</div>
												</div>
												<!--/span-->
											</div>
											<!--/row-->
											<div class="row">
												<div class="col-md-6">
													<div class="row">
														<label class="control-label text-right col-md-4">Lens Side:</label>
														<div class="col-md-8">
															<p class="form-control-static"><code class="text-inverse font-14 font-bold">'.$mr_data->lens_side.'</code></p>
														</div>
													</div>
												</div>
												<!--/span-->
												<div class="col-md-6">
													<div class="row">
														<label class="control-label text-right col-md-4">Frame Type:</label>
														<div class="col-md-8">
															<p class="form-control-static"><code class="text-inverse font-14 font-bold">'.$mr_data->frame_type.'</code></p>
														</div>
													</div>
												</div>
												<!--/span-->
											</div>
											<br>
											<h5 class="box-title">Remark</h5>
											<hr class="m-t-0 m-b-20">
											<div class="col-md-12">
												<div class="form-group">
													<p class="form-control-static"><code class="text-inverse font-14 font-bold">'.$mr_data->remark.'</code></p>
												</div>
											</div>
										</div>
											<!--/form-body-->
										<div class="form-actions">
											<div class="row">
												<div class="col-md-6">
													<code class="text-inverse">Examined by: <span class="font-bold font-14">Dr. '.$dr_data->firstname.' '.$dr_data->lastname.'</span></code><br>
													<code class="text-inverse">Examined On: <span class="font-bold font-14">'.$mr_data->created.'</span></code>
												 </div>
												 <div class="col-md-6 text-right">
													<button type="button" class="btn btn-secondary print-mr"> <i class="fa fa-print"></i> Print</button>	
												 </div>
											</div>
										</div>
									</form>
							
						</div>
					</div>';
			echo $output;
		}
		else
		{
			$output.=   '<div class="row">
							<div class="col-md-12">
								<div class="row no-record">
									<div class="col-md-12 text-center" style="padding-bottom: 20px;">
										<code style="font-size: 18px;"><i class="fa fa-file"></i> No record found.</code>
									</div>
								</div>
							</div>
					   </div>';
			echo $output;
		}
	}
}
?>
