<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Qr_code extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Qrcode_Model');
		$this->load->add_package_path(APPPATH.'third_party/php_qrcode');
		$this->load->library('php_qrcode');

		date_default_timezone_set('Asia/Manila');
		if (!$this->session->userdata('auth')) {
			redirect('web-login', 'refresh');
		}

	}

	public function index()
	{
		$this->load->view('admin/_main/head');
		$this->load->view('admin/qrcode/sub_css');
		$this->load->view('admin/_main/body_start');
		$this->load->view('admin/_wrapper/wrapper_start');
		$this->load->view('admin/qrcode/qrcode');
		$this->load->view('admin/_wrapper/wrapper_end');
		$this->load->view('admin/_main/main_script');
		$this->load->view('admin/qrcode/sub_js');
		$this->load->view('admin/_main/end_tag');

	}

	public function generate_qrcode(){

		$this->form_validation->set_rules('bcode', 		'Barangay Code', 	'trim|required|htmlspecialchars|exact_length[3]');
		$this->form_validation->set_rules('qrcount', 	'QR-count', 		'trim|required|numeric|greater_than[0]|less_than[1001]');

		if($this->form_validation->run() === FALSE)
			echo json_encode(array('status' => 400, 'response' => validation_errors()));
		else
		{
			$data = array();
			$generated_qrcode = array();
			$data['bcode']      = $this->input->post('bcode', TRUE);
			$data['qrcount']	= $this->input->post('qrcount', TRUE);
			$save_folder = 'images'.DIRECTORY_SEPARATOR;
			$saveandprint = FALSE;
			$outputformat = 'png';

			for ($x = 0; $x < $data['qrcount']; $x++) {
				$qr_content = array(
					'data' 			=> $data['bcode'].'-'.date('njgis'). $x,
					'save_folder' 	=> $save_folder,
					'save_name' 	=> $data['bcode'].'-'.date('njgis'). $x,
					'level' 		=> 'L',
					'size' 			=> 5,
					'margin' 		=> 1,
					'saveandprint' 	=> $saveandprint,
					'outputformat' 	=> $outputformat
				);
				$qrcode = array(
					'qr_code' 		=> $data['bcode'].'-'.date('njgis'). $x
				);
				$generated_qrcode[] = $qrcode;
				$data['qr_code'][] 	= $data['bcode'].'-'.date('njgis'). $x;
				$data['qr_data'][] 	= $this->php_qrcode->generate($qr_content);
			}

			$this->Qrcode_Model->insert_generated_qrcode($generated_qrcode);
			$this->load->view('admin/qrcode/print/print', $data);
//			$mpdf = new \Mpdf\Mpdf();
//			$html = $this->load->view('admin/qrcode/print/print', $data, true);
//			$mpdf->WriteHTML($html);
//			$output = 'myLguBaao_' . date('YmdHis') . '.pdf';
//			$mpdf->Output($output, 'I');

		}





	}

}
