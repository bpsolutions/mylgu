<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_List extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_List_Model');
		date_default_timezone_set('Asia/Manila');
		if(!$this->session->userdata('auth')){
			redirect('web-login', 'refresh');
		}

	}

	public function index()
	{
		$this->load->view('admin/_main/head');
		$this->load->view('admin/user_list/sub_css');
		$this->load->view('admin/_main/body_start');
		$this->load->view('admin/_wrapper/wrapper_start');
		$this->load->view('admin/user_list/user_list');
		$this->load->view('admin/_wrapper/wrapper_end');
		$this->load->view('admin/_main/main_script');
		$this->load->view('admin/user_list/sub_js');
		$this->load->view('admin/_main/end_tag');
	}

	public function get_user_list()
	{
		$users = $this->User_List_Model->get_user_list();
		$data = array();
		$counter = 0;
		foreach($users as $li)
		{
			$street 	= $li->street ? $li->street . ' ' : '';
			$barangay 	= $li->barangay ? $li->barangay . ' ' : '';
			$city 		= $li->city ? $li->city . ' ': '';
			$province 	= $li->province ? $li->province . ' ': '';

			$row = array();
			$counter++;
			$row['count']  			= $counter;
			$row['qrcode']  		= $li->user_qrcode;
			$row['name']  			= $li->firstname . ' ' . $li->lastname;
			$row['age']  			= $li->age;
			$row['gender']  		= $li->gender=='M' ? 'Male': 'Female';
			$row['address']  		= $street . $barangay . $city . $province;
			$row['contact']   		= $li->contact;
			$data[]     			= $row;
		}
		echo json_encode($data);
	}



}
?>
