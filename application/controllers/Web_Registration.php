<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Twilio\Rest\Client;
class Web_Registration extends CI_Controller {
	private $sid;
	private $token;
	public function __construct()
	{
		parent::__construct();
		$this->sid = "AC95444463455ef70d892c6f2dd086a5ff";
		$this->token = "d791a4a05790db309a2fcae994937389";
		$this->trial_num = "+12512610663";
		$this->test_receiver = "+639305855791";
		$this->load->model('Registration_Model');
	}

	public function index()
	{
		$this->load->view('public/_main/main_head');
		$this->load->view('public/registration/sub_css');
		$this->load->view('public/_main/body_start');
		$this->load->view('public/_wrapper/wrapper_start');

		$this->load->view('public/registration/registration');

		$this->load->view('public/_wrapper/wrapper_end');
		$this->load->view('public/_main/main_script');
		$this->load->view('public/registration/sub_script');
		$this->load->view('public/_main/end_tag');
	}

	public function web_register()
	{
		$this->form_validation->set_rules('qr_code', 	'QR Code', 				'trim|htmlspecialchars|max_length[20]|is_unique[users.user_qrcode]');
		$this->form_validation->set_rules('firstname', 	'Firstname', 			'trim|required|htmlspecialchars|max_length[60]');
		$this->form_validation->set_rules('middlename', 'Middlename', 			'trim|htmlspecialchars|max_length[60]');
		$this->form_validation->set_rules('lastname', 	'Lastname', 			'trim|required|htmlspecialchars|max_length[60]');
		$this->form_validation->set_rules('age', 		'Age', 					'trim|required|htmlspecialchars|max_length[3]');
		$this->form_validation->set_rules('gender', 	'gender', 				'trim|required|htmlspecialchars');
		$this->form_validation->set_rules('contact', 	'Contact', 				'trim|required|is_unique[users.contact]|max_length[20]',array('is_unique' => '%s is already been used.'));
		$this->form_validation->set_rules('street', 	'Street/Zone', 			'trim|required|htmlspecialchars|max_length[60]');
		$this->form_validation->set_rules('barangay', 	'Barangay', 			'trim|required|htmlspecialchars|max_length[60]');
		$this->form_validation->set_rules('city', 		'City/Municipality', 	'trim|required|htmlspecialchars|max_length[60]');
		$this->form_validation->set_rules('province', 	'Province', 			'trim|required|htmlspecialchars|max_length[60]');
		$this->form_validation->set_rules('zip_code', 	'Zip Code', 			'trim|required|htmlspecialchars|max_length[10]');
		$this->form_validation->set_rules('password', 	'Password', 			'trim|required|min_length[8]');
		$this->form_validation->set_rules('conf_pass',	'Confirm Password', 	'trim|required|matches[password]');
		if($this->form_validation->run() === FALSE)
			echo json_encode(array('status' => 400, 'response' => validation_errors()));
		else
		{
			$qrcode = $this->input->post('qr_code', TRUE);
			if(empty($qrcode) || ctype_space($qrcode)){
				$qrcode = 'SYS'.'-'.date('njgis'). mt_rand(0,9);
				$is_valid = true;
			}
			else{
				$is_valid = $this->Registration_Model->is_valid_qr($qrcode);
			}

			if($is_valid){
				$user_data = array(
					'user_qrcode'           => $qrcode,
					'firstname'             => $this->input->post('firstname', TRUE),
					'middlename'         	=> $this->input->post('middlename', TRUE),
					'lastname'          	=> $this->input->post('lastname', TRUE),
					'age '         			=> $this->input->post('age', TRUE),
					'gender'      			=> $this->input->post('gender', TRUE),
					'contact'               => $this->input->post('contact', TRUE),
					'street'               	=> $this->input->post('street', TRUE),
					'barangay'              => $this->input->post('barangay', TRUE),
					'city'               	=> $this->input->post('city', TRUE),
					'province'              => $this->input->post('province', TRUE),
					'zip_code'              => $this->input->post('zip_code', TRUE),
					'password'  			=> password_hash($this->input->post('password', TRUE), PASSWORD_BCRYPT),
					'conf_number'			=> mt_rand(100000,999999)
				);
				$this->Registration_Model->insert_user($user_data);
				$this->_send_confirmation_code($user_data['conf_number']);
				echo json_encode(array('status' => 200, 'response' => 'Success'));
			}
			else{
				echo json_encode(array('status' => 404, 'response' => 'Invalid QR-code input!'));
			}

		}
	}

	private function _send_confirmation_code($conf_num)
	{
		$client = new Client($this->sid, $this->token);
		$client->messages->create($this->test_receiver,
			array(
				'from' => $this->trial_num,
				'body' => 'Your number confirmation number: '. $conf_num)
		);
	}


}
