<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin_Model extends CI_Model
{
	public function get_establishment()
	{
		$this->db->join('users u', 'u.user_id = e.user_id', 'left');
		$query = $this->db->get('establishment e');
		return $query->result();
	}


	public function is_establishment_exist($id)
	{
		$query = $this->db->get_where('establishment', array('establishment_id' => $id));
		return $query->num_rows() === 1 ? TRUE : FALSE;
	}

	public function update_establishment_status($id, $status)
	{
		$this->db->set('is_verified', $status);
		$this->db->where('establishment_id', $id);
		$this->db->update('establishment');
		return $this->db->affected_rows();
	}

	public function get_establishment_log($id)
	{
		$this->db->join('users u', 'u.user_id = l.public_user_id', 'left');
		$query = $this->db->order_by('l.sign_in_date', 'DESC')->get_where('log l', array('l.establishment_id' => $id));
		return $query->result();
	}
	public function get_establishment_name($id)
	{
		$query = $this->db->get_where('establishment', array('establishment_id' => $id));
		return $query->row()->establishment_name;
	}



	//BORDER CONTROL

	public function get_border_control()
	{
		$query = $this->db->get('border_control');
		return $query->result();
	}

	public function insert_border_control($data)
	{
		$this->db->insert('border_control', $data);
	}

	public function update_border_control($id, $data)
	{
		$this->db->update('border_control', $data, array('bc_id' => $id ));
	}

	public function is_user_exist($contact)
	{
		$query = $this->db->get_where('users', array('contact' => $contact));
		return $query->num_rows() === 1 ? TRUE : FALSE;
	}

	public function user_have_no_role($contact)
	{
		$query = $this->db->get_where('users', array('contact' => $contact));
		return $query->row()->user_type === '0' ? TRUE : FALSE;
	}

	public function insert_bcp($data)
	{
		$this->db->insert('border_control_personnel', $data);
	}

	public function update_user_type($contact)
	{
		$this->db->set('user_type', '2');
		$this->db->where('contact', $contact);
		$this->db->update('users');
		return $this->db->affected_rows();
	}

	public function count_bcp($id)
	{
		$query = $this->db->get_where('border_control_personnel', array('bc_id' => $id));
		return $query->num_rows();
	}

	public function get_bcp($id)
	{
		$this->db->join('users u', 'u.contact = bcp.bcp_contact', 'left');
		$query = $this->db->get_where('border_control_personnel bcp', array('bcp.bc_id' => $id));
		return $query->result();
	}







}
?>
