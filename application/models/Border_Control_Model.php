<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Border_Control_Model extends CI_Model
{
	public function is_vehicle_exist($vehicle_qrcode)
	{
		$query = $this->db->get_where('vehicle', array('vehicle_qrcode' => $vehicle_qrcode));
		return $query->num_rows() === 1 ? TRUE : FALSE;
	}

	public function insert_vehicle($data)
	{
		$this->db->insert('vehicle', $data);
		return $this->db->insert_id();
	}

	public function is_vehicle_id_exist($vehicle_id)
	{
		$query = $this->db->get_where('vehicle', array('vehicle_id' => $vehicle_id));
		return $query->num_rows() === 1 ? TRUE : FALSE;
	}

	public function insert_destination($data)
	{
		$this->db->insert('destination', $data);
		return $this->db->insert_id();
	}

	public function is_passenger_exist($passenger_qrcode)
	{
		$query = $this->db->get_where('users', array('user_qrcode' => $passenger_qrcode));
		return $query->num_rows() === 1 ? TRUE : FALSE;
	}

	public function get_passenger_user_id($passenger_qrcode)
	{
		$query = $this->db->get_where('users', array('user_qrcode' => $passenger_qrcode));
		return $query->row()->user_id;
	}

	public function is_destination_exist($destination_id)
	{
		$query = $this->db->get_where('destination', array('destination_id' => $destination_id));
		return $query->num_rows() === 1 ? TRUE : FALSE;
	}

	public function is_user_exist($user_id)
	{
		$query = $this->db->get_where('users', array('user_id' => $user_id, 'status' => '1'));
		return $query->num_rows() === 1 ? TRUE : FALSE;
	}

	public function insert_passenger($data)
	{
		$this->db->insert('passenger', $data);
		return $this->db->insert_id();
	}


	public function get_passenger_list($destination_id)
	{
		$this->db->join('users u', 'u.user_id = p.user_id', 'left');
		$query = $this->db->get_where('passenger p', array('p.destination_id' => $destination_id));
		return $query->result();
	}

}
?>
