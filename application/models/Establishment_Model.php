<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Establishment_Model extends CI_Model
{
	public function is_user_exist($user_id)
	{
		$query = $this->db->get_where('users', array('user_id' => $user_id));
		return $query->num_rows() === 1 ? TRUE : FALSE;
	}

	public function is_verified_user($user_id)
	{
		$query = $this->db->get_where('users', array('user_id' => $user_id));
		return $query->row()->status === '1' ? TRUE : FALSE;
	}

	public function insert_establishment($data)
	{
		$this->db->insert('establishment', $data);
		return $this->db->insert_id();
	}

	public function have_establishment($user_id)
	{
		$query = $this->db->get_where('establishment', array('user_id' => $user_id));
		return $query->num_rows() > 0 ? TRUE : FALSE;
	}

	public function get_establishment_list($user_id)
	{
		$query = $this->db->get_where('establishment', array('user_id' => $user_id));
		return $query->result();
	}

}
?>
