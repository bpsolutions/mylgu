<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login_Model extends CI_Model
{

	public function is_user_exist($contact)
	{
		$query = $this->db->get_where('users', array('contact' => $contact));
		return $query->num_rows() === 1 ? TRUE : FALSE;
	}

	public function get_user_password($contact)
	{
		$query = $this->db->get_where('users', array('contact' => $contact));
		return $query->row()->password;
	}

	public function get_user_data($contact)
	{
		$query = $this->db->get_where('users', array('contact' => $contact));
		return $query->row();
	}

	public function get_user_status($contact)
	{
		$query = $this->db->get_where('users', array('contact' => $contact));
		return $query->row()->status === '1' ? TRUE : FALSE;
	}

	public function get_user_type($contact)
	{
		$query = $this->db->get_where('users', array('contact' => $contact));
		return $query->row()->user_type;
	}




}
?>
