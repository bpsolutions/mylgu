<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Personnel_Model extends CI_Model
{

	public function is_user_exist($contact)
	{
		$query = $this->db->get_where('users', array('contact' => $contact));
		return $query->num_rows() === 1 ? TRUE : FALSE;
	}

	public function is_verified_user($contact)
	{
		$query = $this->db->get_where('users', array('contact' => $contact));
		return $query->row()->status === '1' ? TRUE : FALSE;
	}

	public function insert_personnel($data)
	{
		$this->db->insert('personnel', $data);
		return $this->db->insert_id();
	}

	public function is_verified_establishment($establishment_id)
	{
		$query = $this->db->get_where('establishment', array('establishment_id' => $establishment_id));
		return $query->row()->is_verified === '1' ? TRUE : FALSE;
	}

	public function is_establishment_exist($establishment_id)
	{
		$query = $this->db->get_where('establishment', array('establishment_id' => $establishment_id));
		return $query->num_rows() === 1 ? TRUE : FALSE;
	}

	public function have_personnel($establishment_id)
	{
		$query = $this->db->get_where('personnel', array('establishment_id' => $establishment_id));
		return $query->num_rows() > 0 ? TRUE : FALSE;
	}

	public function get_personnel_list($establishment_id)
	{
		$this->db->join('users u', 'u.contact = p.contact', 'left');
		$query = $this->db->get_where('personnel p', array('p.establishment_id' => $establishment_id));
		return $query->result();
	}

}
?>
