<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Profile_Model extends CI_Model
{
	public function get_user_profile($uid)
	{
		$this->db->join('user_profile p', 'p.user_id = u.user_id', 'left');
		$query = $this->db->get_where('users u', array('u.user_id' => $uid));
		return $query->result();
	}

	public function update_user_profile($uid, $data)
	{
		$this->db->where('user_id', $uid);
		$this->db->update('user_profile', $data);
	}
	
	public function get_medical_record($uid)
	{
		$this->db->join('user_profile up', 'up.user_id = mr.patient_id', 'left');
		$query = $this->db->get_where('medical_record mr', array('mr.patient_id' => $uid));
		return $query->row();
	}

	public function get_doctor_info($uid)
	{
		$query = $this->db->get_where('user_profile', array('user_id' => $uid));
		return $query->row();
	}
    
}
?>
