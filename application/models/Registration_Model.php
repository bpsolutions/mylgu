<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Registration_Model extends CI_Model
{
	public function insert_user($data){
		$this->db->insert('users', $data);
		return $this->db->insert_id();
	}
	public function is_valid_qr($qr_code){
		$query = $this->db->get_where('generated_qrcode', array('qr_code' => $qr_code));
		return $query->num_rows() === 1 ? TRUE : FALSE;
	}



}
?>
