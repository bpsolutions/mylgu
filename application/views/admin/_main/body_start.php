<body class="fix-header card-no-border fix-sidebar bg-color">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader" style="background: #ffffff7d;">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Mobile Monitoring System</p>
        </div>
    </div>
