<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="kite">
	<!--Favicon-->
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/public/images/icons/favicon.png" type="image/x-icon">
	<link rel="icon" href="<?php echo base_url();?>assets/public/images/icons/favicon.png" type="image/x-icon">

	<title>Mobile Monitoring Application</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>assets/admin/node_modules/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
	<link href="<?php echo base_url();?>assets/admin/node_modules/ps/perfect-scrollbar.min.css" rel="stylesheet"/>
</head>
