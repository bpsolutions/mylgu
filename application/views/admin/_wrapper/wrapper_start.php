	<div id="main-wrapper">
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
				<div class="navbar-header">
					<a class="navbar-brand" href="<?php echo site_url();?>">
						<b><img class="light-logo" src="<?php echo base_url();?>assets/admin/images/logo/logo-light.png" width="50px" height="50px" alt="Mobile Monitoring System" /></b>
					</a>
				</div>
                <div class="navbar-collapse">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up waves-effect waves-dark" href="javascript:void(0)" data-vivaldi-spatnav-clickable="1"><i class="fa fa-bars"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down waves-effect waves-dark" href="javascript:void(0)" data-vivaldi-spatnav-clickable="1"><i class="fa fa-bars"></i></a> </li>
                    </ul>
                    <ul class="navbar-nav my-lg-0">
                        <!-- Profile -->
                        <li class="nav-item dropdown u-pro">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic text-right m-r-10" href="" style="line-height:0px !important;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="<?php echo base_url();?>assets/admin/images/users/admin-male.png" alt="user" class="">
								<span class="hidden-md-down">
									<?php echo $this->session->auth['firstname'].' ';?><i class="fa fa-angle-down"></i></span><br>
									<span class="cus-label label label-danger" style="font-size:10px; margin-top:5px;">Administrator</span>

                            </a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="<?php echo base_url();?>assets/admin/images/users/admin-male.png" alt="<?php echo $this->session->auth['firstname'];?>"></div>
                                            <div class="u-text p-t-20">
                                                <h4 class="btn-rounded btn-inverse btn-sm"><?php echo $this->session->auth['firstname'] .' '. $this->session->auth['lastname'];?></h4>
                                            </div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="<?php echo site_url('logout');?>">
                                            <i class="fa fa-power-off"></i> Logout
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <aside class="left-sidebar">
            <div class="scroll-sidebar active">
				<nav class="sidebar-nav ">
                    <ul id="sidebarnav" class="in">

						<li>
                            <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="icon-Car-Wheel"></i><span class="hide-menu">Dashboard</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php echo site_url('admin');?>">Establishment</a></li>
								<li><a href="<?php echo site_url('border-control');?>">Border Control</a></li>
                            </ul>
                        </li>

						<li>
							<a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="icon-QR-Code"></i><span class="hide-menu">QR-Code</span></a>
							<ul aria-expanded="false" class="collapse">
								<li><a href="<?php echo site_url('Qr_code');?>">Generate QR-code</a></li>
							</ul>
						</li>

						<li>
							<a class="has-arrow waves-effect waves-dark" href="<?php echo site_url('user-list');?>" aria-expanded="false"><i class="icon-User"></i><span class="hide-menu">Users</span></a>
						</li>

                    </ul>
                </nav>
            </div>
        </aside>
    <div class="page-wrapper">
