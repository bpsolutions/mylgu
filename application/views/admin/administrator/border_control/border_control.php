<div class="container-fluid">
	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-themecolor">Border Control</h3>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php echo site_url('admin');?>">Home</a></li>
			</ol>
		</div>
	</div>


	<div class="row">
		<button type="button" class="btn btn-sm btn-outline-info m-b-10"  data-toggle="modal" data-target="#add-bcp-modal"><i class="fa fa-plus"></i> Add Personnel</button>
		<div class="card">
			<div class="card-body">
				<div id="border-control" class="jsgrid" style="position: relative; height: 500px; width: 100%;"></div>
			</div>
		</div>
	</div>

	<!--Add Category Modal-->
	<div id="add-bcp-modal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-border">
					<h4 class="modal-title" id="myModalLabel">Add Personnel</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<form class="" id="add-bcp-form" action="javascript:void(0);">
						<div class="form-group">
							<label class="control-label">Border Control</label>
							<select class="form-control" name="bc" required>
								<?php if(!empty($border_control)){ foreach ($border_control as $bc) {?>
									<option value="<?php echo $bc->bc_id;?>"><?php echo $bc->bc_name;?></option>
								<?php } }?>
							</select>
						</div>


						<div class="form-group">
							<label class="control-label">Contact</label>
							<div class="controls">
								<input type="text" name="contact" class="form-control" required>
							</div>
						</div>


						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<input type="submit" class="btn btn-info" id="bcp-form-submit-btn"  value="Submit">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>
					</form>
				</div>

			</div>
		</div>
	</div>
	<!--End-->

</div>
