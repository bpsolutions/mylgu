<!--script for jsgrid-->
<script type="text/javascript" src="<?php echo base_url();?>assets/admin/node_modules/jsgrid/jsgrid.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/admin/node_modules/jsgrid/jsgrid-init.js"></script>
<!--toast JS-->
<script src="<?php echo base_url();?>assets/global/toast/jquery.toast.js"></script>

<script src="<?php echo base_url();?>assets/admin/js/sidebarmenu.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/custom.min.js"></script>
<script src="<?php echo base_url();?>assets/global/alert/alert.js"></script>
<script type="text/javascript">
	!function(document, window, $){
		//add walk-in user
		$("#border-control").jsGrid({
			height: "500px",
			width: "100%",
			filtering: !1,
			editing: !0,
			inserting: !0,
			sorting: !0,
			paging: !0,
			autoload: !0,
			pageSize: 10,
			pageButtonCount: 5,
			controller: {
				loadData: function(filter) {
					return $.ajax({
						type: "GET",
						url: 'get-border-control',
						dataType: 'json'
					});
				},
				insertItem: function (item) {
					return $.ajax({
						method: "POST",
						url: "add-border-control",
						data: item,
						complete:function(data) {
							$("#border-control").jsGrid("loadData");
							var json = JSON.parse(data.responseText);
							if(json.status == 200)
							{ $.alert_success(json.response); }

							else
							{ $.alert_error(json.response); }
						},
						error: function(data) {console.log(data);}
					});
				},
				updateItem: function (item) {
					return $.ajax({
						method: "POST",
						url: "update-border-control",
						data: item,
						complete:function(data) {
							$("#border-control").jsGrid("loadData");
							var json = JSON.parse(data.responseText);
							if(json.status == 200)
							{ $.alert_success(json.response); }
							else
							{ $.alert_error(json.response); }
						},
						error: function(data) {console.log(data);}
					});
				},
			},
			fields: [
				{
					title: "#",
					name: "count",
					type: "text",
					width: 5,
					inserting: false,
					editing: false
				},
				{
					title: "Name",
					name: "name",
					type: "text",
					width: 40,
					validate: 'required'
				},
				{
					title: "Location",
					name: "location",
					type: "text",
					width: 60,
					validate: 'required'
				},
				{
					title: "Personnel",
					name: "personnel",
					type: "text",
					width: 30,
					inserting: false,
					editing: false,
					filtering: false,
				},
				{
					title: "Created",
					name: "created",
					type: "text",
					width: 40,
					inserting: false,
					editing: false

				},

				{
					type: 'control',
					deleteButton: false,
				}
			]
		});


		//add category function
		$('#add-bcp-form').on('submit', function () {
			$.ajax({
				url: "add-bcp",
				type: 'POST',
				data: $(this).serialize(),
				beforeSend :function () {
					$('.preloader').fadeIn();
					$('#bcp-form-submit-btn').prop("disabled", true);
				},
				complete:function(data) {
					console.log(data)
					$('.preloader').fadeOut();
					var json = JSON.parse(data.responseText);
					if(json.status == 200)
					{
						$.alert_success(json.response);
						setTimeout(function(){ $('#bcp-form-submit-btn').prop("disabled", false); location.reload(); }, 3000);

					}
					else
					{
						$.alert_error(json.response);
						setTimeout(function(){ $('#bcp-form-submit-btn').prop("disabled", false); }, 3000);
					}
				}
			});
			return false;
		});
		//eof


	}(document, window, jQuery);
</script>
