<div class="container-fluid">
	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-themecolor">Establishment Log Report</h3>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php echo site_url('admin');?>">Home</a></li>
			</ol>
		</div>
	</div>

	<div class="row">
		<div class="card">
			<div class="card-body">
				<h3 class="text-center"><?php if(isset($name)) echo $name;?></h3>
				<div id="elr" class="jsgrid" style="position: relative; height: 500px; width: 100%;"></div>
			</div>
		</div>
	</div>
</div>
