<!--script for jsgrid-->
<script type="text/javascript" src="<?php echo base_url();?>assets/admin/node_modules/jsgrid/jsgrid.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/admin/node_modules/jsgrid/jsgrid-init.js"></script>
<!--toast JS-->
<script src="<?php echo base_url();?>assets/global/toast/jquery.toast.js"></script>

<script src="<?php echo base_url();?>assets/admin/js/sidebarmenu.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/custom.min.js"></script>
<script src="<?php echo base_url();?>assets/global/alert/alert.js"></script>
<script type="text/javascript">
!function(document, window, $){
	//add walk-in user
			$("#elr").jsGrid({
				height: "500px",
				width: "100%",
				filtering: !1,
				editing: !1,
				inserting: !1,
				sorting: !0,
				paging: !0,
				autoload: !0,
				pageSize: 10,
				pageButtonCount: 5,
				controller: {
					loadData: function(filter) {
						return $.ajax({
							type: "GET",
							url: '<?php echo site_url('get-establishment-log');?>',
							dataType: 'json'
						});
					},

				},
				fields: [
					{
						title: "#",
						name: "count",
						type: "text",
						width: 5,
					},
					{
						title: "QRCODE",
						name: "qr_code",
						type: "text",
						width: 30,
					},
					{
						title: "Temperature",
						name: "temperature",
						type: "text",
						width: 40,
						align: "center"

					},
					{
						title: "Age",
						name: "age",
						type: "text",
						width: 10,

					},
					{
						title: "Contact",
						name: "contact",
						type: "text",
						width: 40,
					},
					{
						title: "Address",
						name: "address",
						type: "text",
						width: 50,
					},
					{
						title: "Date",
						name: "date",
						type: "text",
						width: 50,
					}
				]
			});

}(document, window, jQuery);
</script>
