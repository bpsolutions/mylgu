<!--script for jsgrid-->
<script type="text/javascript" src="<?php echo base_url();?>assets/admin/node_modules/jsgrid/jsgrid.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/admin/node_modules/jsgrid/jsgrid-init.js"></script>
<!--toast JS-->
<script src="<?php echo base_url();?>assets/global/toast/jquery.toast.js"></script>
<!-- sweet alert -->
<script src="<?php echo base_url();?>assets/global/sweetalert/sweetalert.min.js"></script>


<script src="<?php echo base_url();?>assets/admin/js/sidebarmenu.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/custom.min.js"></script>
<script src="<?php echo base_url();?>assets/global/alert/alert.js"></script>
<script type="text/javascript">
	!function(document, window, $){
		//add walk-in user
		$("#establishment-list").jsGrid({
			height: "500px",
			width: "100%",
			filtering: !1,
			editing: !1,
			inserting: !1,
			sorting: !0,
			paging: !0,
			autoload: !0,
			pageSize: 10,
			pageButtonCount: 5,
			deleteConfirm: "Do you really want to remove this establishment?",
			controller: {
				loadData: function(filter) {
					return $.ajax({
						type: "GET",
						url: 'get-establishment',
						dataType: 'json'
					});
				},
				updateItem: function (item) {
					return $.ajax({
						method: "POST",
						url: "update-establishment-status",
						data: item,
						complete:function(data) {
							$("#establishment-list").jsGrid("loadData");
							var json = JSON.parse(data.responseText);
							if(json.status == 200)
							{ $.alert_success(json.response); }
							else
							{ $.alert_error(json.response); }
						},
						error: function(data) {console.log(data);}
					});
				},

			},
			fields: [
				{
					title: "#",
					name: "count",
					type: "text",
					width: 5,
					editing: false
				},
				{
					title: "Created By",
					name: "created_by",
					type: "text",
					width: 50,
					editing: false
				},
				{
					title: "Establishment Name",
					name: "name",
					type: "text",
					width: 70,
					editing: false
				},
				{
					title: "Address",
					name: "address",
					type: "text",
					width: 50,
					editing: false
				},
				{
					title: 'Permit Number',
					name: 'permit_number',
					type: "text",
					width: 50,
					editing: false
				},
				{
					title: 'Status',
					name: 'str_status',
					type: "text",
					width: 40,
					editing: false
				},
				{
					title: "Action",
					width: 50,
					align: "center",
					itemTemplate: function(_,item) {
						if(item.status=='0')
							return $.action_btn(item, false, false, true);
						else if(item.status=='1')
							return $.action_btn(item, true, true, false);
						else
							return $.action_btn(item, true, true, true);
					}
				}
			]
		});

		//action buttons function
		$.action_btn = function(item, astat, dstat, vstat) {

			var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);

			var $approve_btn = $("<button>").attr("class", "fa fa-check btn btn-sm btn-success m-r-4").attr("disabled", astat)
					.click(function(e) {
						e.stopPropagation();
						swal({
							title: "Verify establishment?",
							type: "info",
							showCancelButton: true,
							confirmButtonText: "Confirm",
							cancelButtonText: "Cancel"
						}).then((value) => {
							if(value.value) {
								$.ajax({
									type: 'POST',
									url: 'verify-establishment',
									data: item,
									complete:function(data) {
										$("#establishment-list").jsGrid("loadData");
										var json = JSON.parse(data.responseText);
										if(json.status == 200)
										{ $.alert_success(json.response); }
										else
										{ $.alert_error(json.response); }
									},
									error: function(data) {console.log(data);}
								});
							}
						});
					});

			var $denied_btn = $("<button>").attr("class", "fa fa-times btn-sm btn btn-danger m-r-4").attr("disabled", dstat)
					.click(function(e) {
						e.stopPropagation();
						swal({
							title: "Denied establishment?",
							type: "warning",
							showCancelButton: true,
							confirmButtonText: "Confirm",
							cancelButtonText: "Cancel"
						}).then((value) => {
							if(value.value) {
								$.ajax({
									type: 'POST',
									url: 'denied-establishment',
									data: item,
									complete:function(data) {
										$("#establishment-list").jsGrid("loadData");
										var json = JSON.parse(data.responseText);
										if(json.status == 200)
										{ $.alert_success(json.response); }
										else
										{ $.alert_error(json.response); }
									},
									error: function(data) {console.log(data);}
								});
							}
						});
					});
			var $view_btn = $("<button>").attr("class", "fa fa-eye btn-sm btn btn-secondary").attr("disabled", vstat)
					.click(function(e) {
						e.stopPropagation();
						$.ajax({
							type: 'post',
							url: 'email-passcode',
							data: item,
							beforeSend :function () {
								$('.preloader').fadeIn();
							},
							complete:function(data) {
								$('.preloader').fadeOut();
								var json = JSON.parse(data.responseText);
								if(json.status == 200) {
									{ $.alert_success(json.response); }
									(async () => {
										const { value: password } = await Swal.fire({
											title: 'Passcode',
											input: 'password',
											inputPlaceholder: 'Enter passcode',
											inputAttributes: {
												autocapitalize: 'off',
												autocorrect: 'off'
											}
										})

										if (password) {
											if(password==json.pcode){
												location.href = '<?php echo site_url('establishment-log-report/');?>' + item.id;
											}
											else{
												$.alert_error("Incorrect passcode!");
											}
										}

									})()

								}
								else {
									$.alert_error(json.response);
								}
							},
							error: function(data) {console.log(data);}
						});

					});


			// $("<a>").attr("class", "fa fa-eye btn-sm btn btn-secondary").attr("disabled", vstat).attr('href',
			// 'establishment-log-report/' + item.id);

			return $result.add($("<div>").append($approve_btn, $denied_btn, $view_btn));
		};

	}(document, window, jQuery);
</script>

