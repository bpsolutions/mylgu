<!DOCTYPE html>
<html>
    <head>
        <title>Logs Passcode</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    </head>
    <body style="color:rgb(97, 97, 97);" >
    <div style="text-align: center; background: #efefef;"> <img src="cid:<?php if(!empty($email_logo)) echo $email_logo;?>" height="60px" width="60px" alt="MMS"/></div><br>

        <table align="center">
            <tr>
                <td class="mobile-padding"><br>
					<h3>Passcode: <?php if(isset($passcode)) echo $passcode;?></h3><br><br>
                </td>
            </tr>
            <tr>
                <td style="text-align:center;">
                    <p style="font-size: 11px;"><span style="color: #26B99A;">MyLGU</span> &#169; 2020. All Rights Reserved.</p><br>
                </td>
            </tr>
        </table>
    </body>
</html>
