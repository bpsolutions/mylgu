<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>MMS</title>
	<style>
		@media print {
			@page {
				sheet-size: 215.9mm 279.4mm;
			}
		}
	</style>
</head>
<body onload="window.print();">
<h3 style="text-align: center;">Barangay Code: <?php if(isset($bcode)){echo $bcode; }?> </h3>
<br>

<?php if(isset($qr_data) && isset($qrcount) && isset($qr_code)){
	for ($x = 0; $x < $qrcount; $x++) {
		echo '<div style="display: inline-grid; margin: 20px;">';
		echo  '<code style="font-size: 14px;"><b> ID: ' . $qr_code[$x] .'</b></code>';

		echo '<span style="text-align: center; margin-top: 10px;"><img src='. $qr_data[$x] .'></span>';
		echo '</div>';


	}
}?>


</body>
</html>
