<div class="container-fluid">
	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-themecolor">QR Code</h3>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php echo site_url('admin');?>">Home</a></li>
			</ol>
		</div>
	</div>

	<div class="row col-6">
		<div class="card">
			<div class="card-body">

						<div class="card card-body">
							<div class="row">
								<div class="col-6"><h3 class="mb-0">Generate QR Code</h3></div>
								<div class="col-6 text-right" style="font-size: 40px;"><i class="text-center icon-QR-Code"></i></div>
							</div>

							<br>
							<div class="row">
								<div class="col-sm-12 col-xs-12">
									<form id="qr-form" action="generate-qrcode" method="post">
										<div class="form-group">
											<label>Barangay Code <small class="help"> e.g. "ABC"</small></label>
											<input type="text" name="bcode" class="form-control" minlength="3" maxlength="3" placeholder="Barangay code" pattern="[A-Z]{3}" onkeyup="this.value = this.value.toUpperCase()" required>
											<span class="help-block text-muted"><small>This will be added to the unique id of user in QR-code.</small></span>
										</div>
										<div class="form-group">
											<label>QR Code Count</label>
											<input type="number" name="qrcount" class="form-control" minlength="1" maxlength="1000" placeholder="1-1000" required>
											<span class="help-block text-muted"><small>Number of QR-codes to be generated.</small></span>
										</div>

										<button type="submit" id="btn-gen-qr-btn" class="btn btn-success waves-effect waves-light mr-2">Submit</button>

									</form>
								</div>
							</div>
						</div>





			</div>
		</div>
	</div>
</div>
