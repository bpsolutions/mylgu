<!--script for jsgrid-->
<script type="text/javascript" src="<?php echo base_url();?>assets/admin/node_modules/jsgrid/jsgrid.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/admin/node_modules/jsgrid/jsgrid-init.js"></script>
<!--toast JS-->
<script src="<?php echo base_url();?>assets/global/toast/jquery.toast.js"></script>
<!-- sweet alert -->
<script src="<?php echo base_url();?>assets/global/sweetalert/sweetalert.min.js"></script>


<script src="<?php echo base_url();?>assets/admin/js/sidebarmenu.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/custom.min.js"></script>
<script src="<?php echo base_url();?>assets/global/alert/alert.js"></script>
<script type="text/javascript">
!function(document, window, $){
	//add walk-in user
			$("#user-list").jsGrid({
				height: "500px",
				width: "100%",
				filtering: !1,
				editing: !1,
				inserting: !1,
				sorting: !0,
				paging: !0,
				autoload: !0,
				pageSize: 10,
				pageButtonCount: 5,
				deleteConfirm: "Do you really want to remove this establishment?",
				controller: {
					loadData: function(filter) {
						return $.ajax({
							type: "GET",
							url: 'get-user-list',
							dataType: 'json'
						});
					},

				},
				fields: [
					{
						title: "#",
						name: "count",
						type: "text",
						width: 5,

					},
					{
						title: 'QR-Code',
						name: 'qrcode',
						type: "text",
						width: 30,

					},
					{
						title: "name",
						name: "name",
						type: "text",
						width: 50,

					},
					{
						title: "Age",
						name: "age",
						type: "text",
						width: 5,

					},
					{
						title: "Gender",
						name: "gender",
						type: "text",
						width: 30,

					},
					{
						title: 'Contact',
						name: 'contact',
						type: "text",
						width: 35,

					},
					{
						title: 'Address',
						name: 'address',
						type: "text",
						width: 60,

					},


				]
			});



}(document, window, jQuery);
</script>
