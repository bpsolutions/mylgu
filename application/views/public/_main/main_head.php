<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Mobile Monitoring Application</title>

	<!-- mobile responsive meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Slick Carousel -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/public/plugins/slick/slick.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/public/plugins/slick/slick-theme.css">
	<!-- FancyBox -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/public/plugins/fancybox/jquery.fancybox.min.css">
	<!-- Stylesheets -->
	<link href="<?php echo base_url();?>assets/public/css/style.css" rel="stylesheet">

	<!--sweetalert css-->
	<link href="<?php echo base_url();?>assets/global/sweetalert/sweetalert.css" rel="stylesheet">

	<link href="<?php echo base_url();?>assets/global/spinners/spinners.css" rel="stylesheet">

	<!--Favicon-->
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/public/images/icons/favicon.png" type="image/x-icon">
	<link rel="icon" href="<?php echo base_url();?>assets/public/images/icons/favicon.png" type="image/x-icon">
