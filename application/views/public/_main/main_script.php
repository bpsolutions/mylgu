<script src="<?php echo base_url();?>assets/public/plugins/jquery.js"></script>
<script src="<?php echo base_url();?>assets/public/plugins/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/public/plugins/bootstrap-select.min.js"></script>
<!-- Slick Slider -->
<script src="<?php echo base_url();?>assets/public/plugins/slick/slick.min.js"></script>
<!-- FancyBox -->
<script src="<?php echo base_url();?>assets/public/plugins/fancybox/jquery.fancybox.min.js"></script>
<!--sweetalert js-->
<script src="<?php echo base_url();?>assets/global/sweetalert/sweetalert.min.js"></script>
<!--printArea JS-->
<script src="<?php echo base_url();?>assets/admin/js/jquery.PrintArea.js"></script>
<script src="<?php echo base_url();?>assets/public/plugins/validate.js"></script>
<script src="<?php echo base_url();?>assets/public/plugins/wow.js"></script>
<script src="<?php echo base_url();?>assets/public/plugins/jquery-ui.js"></script>
<script src="<?php echo base_url();?>assets/public/plugins/timePicker.js"></script>
<script src="<?php echo base_url();?>assets/public/js/script.js"></script>
<script src="<?php echo base_url();?>assets/public/js/logout.js"></script>
<!--cart js-->
<script src="<?php echo base_url();?>assets/public/js/cart.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		//get medical record
		$('#mr-modal-btn').on('click', function (){
			$.ajax({
				type: "get",
				url: "get-mr-data",
				beforeSend :function () {
					$('.preloader').fadeIn();
				},
				success :function(data){
					$('.preloader').fadeOut();
					$('.mr-content').html(data);
				}
			});
		});
		//eof
		//print medical record
		$(document).on('click','.print-mr',function(){
			var mode = 'iframe'; //popup
			var close = mode == "popup";
			var options = {
				mode: mode,
				popClose: close
			};
			$("div.mr-content").printArea(options);
		});
		//eof
	});
</script>
