<!--footer-main-->
<footer class="footer-main" style="margin-top: 10rem;">
	<div class="footer-top">
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-sm-6 col-xs-12">
					<div class="about-widget">
						<div class="footer-logo">
							<figure>
								<a href="index.html">
									<img src="<?php echo base_url();?>assets/public/images/logo/public-logo.png" alt="" width="100">
								</a>
							</figure>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>


					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<h6>Follow Us</h6>
					<ul class="list-inline social-icons">
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#"><i class="fa fa-vimeo"></i></a></li>
					</ul>
				</div>




				<div class="col-md-4 col-sm-6 col-xs-12">
						<h6>Contact Us</h6>
						<ul class="location-link">
							<li class="item">
								<i class="fa fa-map-marker"></i>
								<p>Traders Square, Building P. Burgos St, Naga, 4400 Camarines Sur</p>
							</li>
							<li class="item">
								<i class="fa fa-envelope-o" aria-hidden="true"></i>
								<a href="#">
									<p>kite.dev@gmail.com</p>
								</a>
							</li>
							<li class="item">
								<i class="fa fa-phone" aria-hidden="true"></i>
								<p>0928 559 5988</p>
							</li>
						</ul>

				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom">
		<div class="container clearfix">
			<div class="copyright-text">
				<p> © <script>
						document.write(new Date().getFullYear())
					</script> KITE

				</p>
			</div>

		</div>
	</div>
</footer>
<!--End footer-main-->

</div>
<!--End pagewrapper-->


<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".header-top">
	<span class="icon fa fa-angle-up"></span>
</div>
