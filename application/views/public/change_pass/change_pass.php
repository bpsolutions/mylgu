<section class="pb-0 text-center">
	<div class="container">
		<div class="contact-area login-box" style="padding-left: unset; width: " >
			<div class="section-title" style="padding-top: 3%;">
				<h3><b>Change Password</b></h3>
			</div>


			<form id="change-pass-form" action="javascript:void(0)"  class="default-form contact-form" style="margin-top: 5%;">
				<div class="row" style="padding: 1% 5% 5% 5%;">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
							<input type="password" class="form-control"  name="old_pass" placeholder="Old Password" required="" autocomplete="off">
						</div>

						<div class="form-group">
							<input type="password" class="form-control" minlength="8" name="new_pass" placeholder="New Password" required="" autocomplete="off">
						</div>

						<div class="form-group">
							<input type="password" class="form-control"  name="conf_new_pass" placeholder="Confirm Password" required="" autocomplete="off">
						</div>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12">
						<br>
						<div class="form-group text-center">
							<button type="submit" id="change-pass-btn" class="btn-style-one">submit</button>
						</div>
					</div>
				</div>
			</form>



		</div>
	</div>
</section>
<!-- End Contact Section -->
