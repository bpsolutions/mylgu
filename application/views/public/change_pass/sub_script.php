<script type="text/javascript">
	!function(document, window, $) {
		$('#change-pass-form').on('submit', function (){
			// swal.showLoading();
			$.ajax({
				type: "post",
				url: "change-password",
				beforeSend :function () { $('#change-pass-btn').attr("disabled", true); },
				data: $(this).serialize(),
				dataType: "JSON",
				timeout: 30000,
			}).done(function(data){
				if(data.stat == false)
				{
					swal("Error!", data.res, "error");
					$('#change-pass-btn').attr("disabled", false);
				}
				else
					{
						const Toast = Swal.mixin({
							toast: true,
							position: 'top-end',
							showConfirmButton: false,
							timer: 3000,
							onClose: () => {
								location.href = '<?php echo site_url('logout');?>';
								$('#change-pass-btn').attr("disabled", false);
							}
						});
						Toast.fire({
							type: 'success',
							title: data.res
						});
					}
			}).fail(function(jqXHR, textStatus){
				$('#change-pass-btn').attr("disabled", false);
				if(textStatus == 'timeout')
					swal("Error!", 'Slow internet connection. Please try again later', "error");
			});
		});

	}(document, window, jQuery);
</script>
