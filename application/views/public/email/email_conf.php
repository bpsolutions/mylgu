<!DOCTYPE html>
<html>
    <head>
        <title>Mariano Optical Center - Email Confirmation</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    </head>
    <body style="color:rgb(97, 97, 97);" >
    <div style="text-align: center; background: #efefef;"> <img src="cid:<?php if(!empty($email_logo)) echo $email_logo;?>" height="60px" width="120px" alt="Business Directory"/></div><br>
    <h3><?php if(!empty($firstname) && !empty($lastname)) echo 'Hi '.$firstname.' '.$lastname.','; else echo 'Hi,'; ?></h3>
        <center>To complete the activation of your account, a verification is needed. Click on the button below to verify your account.</center><br>
        <a target="_blank" href="<?php if(!empty($email)&& !empty($conf_code)) echo site_url("verify-email?email=". $email . "&conf_code=".$conf_code.""); ?>" style="padding: 9px 15px; text-align: center; font-size: 20px; display: block; border-radius: 3px; color: white; text-decoration: none; background: rgb(51, 189, 160); margin-left: 35%; margin-right: 35%; ">Verify</a>
        <table align="center">
            <tr>
                <td class="mobile-padding"><br>
                    Thank you! Please <a style="color: rgba(48, 131, 201, 1);" href="<?php echo site_url();?>">visit our website</a> for further information.<br><br><br>
                </td>
            </tr>
            <tr>
                <td style="text-align:center;">
                    <p style="font-size: 11px;"><span style="color: #26B99A;">Mariano</span> <span style="color: #599cd4;"> Optical Center</span> &#169; 2019. All Rights Reserved.</p><br>
                </td>
            </tr>
        </table>
    </body>
</html>
