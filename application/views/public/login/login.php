<section class="pb-0 text-center">
	<div class="container">
		<div class="contact-area login-box" style="padding-left: unset; width: " >
			<div class="section-title" style="padding-top: 3%;">
				<h3><b>Login</b></h3>
			</div>
			<form id="login-form" action="javascript:void(0)"  class="default-form contact-form" style="margin-top: 5%;">
				<div class="row" style="padding: 1% 5% 5% 5%;">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
							<input type="text" class="form-control"  name="contact" placeholder="Contact" required="">
						</div>

						<div class="form-group">
							<input type="password" class="form-control"  name="password" placeholder="Password" required="">
						</div>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12">
						<br>
						<div class="form-group text-center">
							<button type="submit" id="login-btn" class="btn-style-one">Submit</button>
						</div>
						<a href="web-registration"><span class="text-muted">Don't have an account?</span> Register</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>
<!-- End Contact Section -->
