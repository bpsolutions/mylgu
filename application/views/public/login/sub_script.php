<script type="text/javascript">
	!function(document, window, $) {
		$('#login-form').on('submit', function (){
			swal.showLoading();
			$.ajax({
				type: "post",
				url: "admin-login",
				beforeSend :function () { $('#login-btn').attr("disabled", true); },
				data: $(this).serialize(),
				dataType: "JSON",
				timeout: 30000,
			}).done(function(data){
				console.log(data)
				if(data.status == 200) {
					const Toast = Swal.mixin({
						toast: true,
						position: 'top-end',
						showConfirmButton: false,
						timer: 3000,
						onClose: () => {
							$('#login-btn').attr("disabled", false);
							location.href = '<?php echo site_url('admin');?>'
						}
					});
					Toast.fire({ type: 'success', title: 'Signed in successfully'});
				}
				else {
					$('#login-btn').attr("disabled", false);
					swal("Error!", data.response, "error");
				}
			}).fail(function(jqXHR, textStatus){
				$('#login-btn').attr("disabled", false);
				if(textStatus == 'timeout')
					swal("Error!", 'Slow internet connection. Please try again later', "error");
			});
		});

	}(document, window, jQuery);
</script>
