<section class="pb-0 text-center">
	<div class="container">
		<div class="contact-area box" style="padding-left: unset;">
			<div class="section-title" style="padding-top: 3%;">
				<h3><b>Registration</b></h3>
			</div>
			<form id="reg-form" action="javascript:void(0)" class="default-form contact-form" style="margin-top: 5%;">
				<div class="row" style="padding: 1% 5% 5% 5%;">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="form-group">
							<input type="text" class="form-control" name="qr_code"  maxlength="20" placeholder="Given QR-code">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="firstname"  maxlength="60" placeholder="Firstname" required="">
						</div>

						<div class="form-group">
							<input type="text" class="form-control" name="middlename"  maxlength="60" placeholder="Middlename">
						</div>

						<div class="form-group">
							<input type="text" class="form-control" name="lastname"  maxlength="60" placeholder="Lastname" required="">
						</div>

						<div class="form-group">
							<input type="number" class="form-control" name="age" maxlength="3" placeholder="Age" required="">
						</div>
						<div class="form-group">
							<select name="gender"  required>
								<option value="" disabled selected>Gender</option>
								<option value="M">Male</option>
								<option value="F">Female</option>
							</select>
						</div>
						<div class="form-group">
							<input type="text" name="contact" class="form-control"  maxlength="20" placeholder="Contact" pattern="^(09|\+639)\d{9}$" required="">
						</div>

					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="form-group">
							<input type="text" name="street" class="form-control"  maxlength="60" placeholder="Street">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="barangay"  maxlength="60" placeholder="Barangay" required>
						</div>
						<div class="form-group">
							<input type="text"  class="form-control" name="city"  maxlength="60" placeholder="City" required="">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="province" maxlength="60" placeholder="Province" required="">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="zip_code" maxlength="10" placeholder="Zip Code" required="">
						</div>
						<div class="form-group">
							<input type="password" class="form-control" name="password"  minlength="8" placeholder="Password" required="">
						</div>
						<div class="form-group">
							<input type="password" class="form-control" name="conf_pass"  placeholder="Confirm Password" required="">
						</div>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12">
						<br>
						<div class="form-group text-center">
							<button type="submit" id="reg-sub-btn" class="btn-style-one">submit now</button>
						</div>
<!--						<a href="login"><span class="text-muted">Already have an account?</span> Login</a>-->
					</div>
				</div>
			</form>
		</div>
	</div>
</section>
<!-- End Contact Section -->
