<!-- custom JS-->
<script type="text/javascript">
	!function(document, window, $) {
		$('#reg-form').on('submit', function () {
			swal.showLoading();
			$.ajax({
				url: "web-register",
				type: 'POST',
				data:  $(this).serialize(),
				dataType : 'json',
				timeout: 30000,
				beforeSend :function () { $('#reg-sub-btn').attr("disabled", true); }
			}).done(function(data){
				$('#reg-sub-btn').attr("disabled", false);
				if(data.status == 200) {
					swal("Success!", data.response, "success")
							.then(function() {location.reload()});
				}
				else{
					swal("Error!", data.response, "error");
				}

			}).fail(function(jqXHR, textStatus){
				$('#reg-sub-btn').attr("disabled", false);
				if(textStatus == 'timeout')
					swal("Error!", 'Slow internet connection. Please try again later', "error");
			});
		});
	}(document, window, jQuery);
</script>
