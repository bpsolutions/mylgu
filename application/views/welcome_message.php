<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>CodeIgniter - PHP QR Code</title>
	</head>
	<body onload="window.print();">
		<div class="container">
			<h1>TEXT</h1>
			<p><img src="<?php echo base_url($qrc_text); ?>"></p>


		</div>

		<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
	</body>
</html>
