
!function($) {
	"use strict";
	//set alert function
	$.alert_success = function(res)
	{
		$.toast({ heading: 'Success!', text: res, position: 'top-right', loaderBg:'#ff6849', icon: 'success', hideAfter: 4000, stack: 3 });
	};

	$.alert_error = function(res)
	{
		$.toast({ heading: 'Error!', text: res, position: 'top-right', loaderBg:'#ff6849', icon: 'error', hideAfter: 4000, stack: 3 });
	};

}(window.jQuery);
