$(document).ready(function(){
	//load cart cart function
	$('#cart-content').load("load-cart");
	//eof

	//check cart count function
	check_cart_count();
	//eof

	//check reservation count function
	check_reservation_count();
	//eof

	//check appointment count function
	check_appointment_count();
	//eof

	//add to cart function
	$(document).on('click', '.reserve-btn',function(e){
		e.preventDefault();
		var pid = $(this).data('pid');
		var qty = $(this).data('qty');
		swal({
			title: "Do you want to reserve this item?",
			type: "info",
			input: 'number',
			inputAttributes: { min: 1, max: qty },
			inputPlaceholder: 'Quantity',
			showCancelButton: true,
			confirmButtonText: "Confirm",
			cancelButtonText: "Cancel",
			inputValidator: (value) =>
			{
				return new Promise((resolve) => {
					if (value)
					{
						if(value >= 1 && value <= qty )
						{
							$.ajax({
								url : "add-to-cart",
								method : "POST",
								data : {pid:pid, qty:value},
								success: function(data) {
									$('#cart-content').html(data);
									$.alert_success('Added to cart!');
									setTimeout(() => {
										resolve(swal.close())
									}, 3000);
									check_cart_count();
								}
							});
						}
						else
						{
							resolve('Out of range!')
						}
					}
					else
					{
						resolve('Quantity is required!')
					}
				})
			}
		});
	});
	//eof

	//remove to cart function
	$(document).on('click','.remove-to-cart',function(e){
		e.preventDefault();
		var row_id = $(this).data('rowid');
		swal({
			title: "Remove to cart?",
			type: "info",
			showCancelButton: true,
			confirmButtonText: "Confirm",
			cancelButtonText: "Cancel"
		}).then((value) => {
			if (value.value) {
				$.ajax({
					url : "remove-to-cart",
					method : "POST",
					data : {row_id : row_id},
					success :function(data){
						$('#cart-content').html(data);
						check_cart_count();
					}
				});
			}
		});
	});
	//eof

	//reserve product function
	$(document).on('click','#reserve-product-btn',function(e){
		e.preventDefault();
		swal({
			title: "Reserve all items in you cart?",
			type: "info",
			showCancelButton: true,
			confirmButtonText: "Confirm",
			cancelButtonText: "Cancel"
		}).then((value) => {
			if (value.value)
			{
				$.ajax({
					url : "add-to-reserve",
					method : "POST",
					beforeSend :function () {
						$('.preloader').fadeIn();
						$('#reserve-product-btn').prop("disabled", true);
					},
					complete: function(data){
						$('.preloader').fadeOut();
						var json = JSON.parse(data.responseText);
						if(json.stat == false)
						{
							$.alert_error(json.res);
							setTimeout(function(){ $('#reserve-product-btn').prop("disabled", false); }, 8000);
						}
						else
						{
							$.alert_success(json.res);
							setTimeout(function(){ $('#reserve-product-btn').prop("disabled", false); location.reload(); }, 8000);
						}
					}
				});
			}
		});
	});
	//eof

	//notify if cart is not empty
	function check_cart_count() {
		$.ajax({
			url : "check-cart-count",
			method : "POST",
			success :function(data){
				$('#notif').html(data);
			}
		});
	}
	//eof

	//notify if has reservation
	function check_reservation_count() {
		$.ajax({
			url : "check-reservation-count",
			method : "POST",
			success :function(data){
				$('.r-count').html(data);
			}
		});
	}
	//eof

	//notify if has reservation
	function check_appointment_count() {
		$.ajax({
			url : "check-appointment-count",
			method : "POST",
			success :function(data){
				$('.a-count').html(data);
			}
		});
	}
	//eof

});
