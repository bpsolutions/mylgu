!function(document, window, $) {
	$('#logout').on('click', function (e){
		e.preventDefault();
		swal({
			title: "Logout now?",
			type: "info",
			showCancelButton: true,
			confirmButtonText: "Confirm",
			cancelButtonText: "Cancel"
		}).then((value) => {
			if(value.value) {
				$.ajax({
					type: "post",
					url: "logout",
					beforeSend :function () { $(this).attr("disabled", true); },
				}).done(function(){
					$(this).attr("disabled", false);
					const Toast = Swal.mixin({
						toast: true,
						position: 'top-end',
						showConfirmButton: false,
						timer: 3000,
						onClose: () => {
							location.reload();
						}
					});
					Toast.fire({
						type: 'success',
						title: 'Logout successfully'
					});
				}).fail(function(jqXHR, textStatus){
					$(this).attr("disabled", false);
					if(textStatus == 'timeout')
						swal("Error!", 'Slow internet connection. Please try again later', "error");
				});
			}
		});
	});
}(document, window, jQuery);
