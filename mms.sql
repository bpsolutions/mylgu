-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 29, 2020 at 07:55 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mms`
--

-- --------------------------------------------------------

--
-- Table structure for table `border_control`
--

CREATE TABLE `border_control` (
  `bc_id` int(11) NOT NULL,
  `bc_name` varchar(255) NOT NULL,
  `bc_location` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `border_control`
--

INSERT INTO `border_control` (`bc_id`, `bc_name`, `bc_location`, `created`, `modified`) VALUES
(1, 'Border Control One', 'Camariner Sur Border Control', '2020-08-23 15:36:59', '2020-08-23 15:56:21'),
(2, 'Border Control Two', 'Bagumbayan Sur Border Control', '2020-08-23 15:43:29', NULL),
(3, 'Test', 'Boundary Canaman', '2020-08-25 00:28:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `border_control_personnel`
--

CREATE TABLE `border_control_personnel` (
  `bcp_id` int(11) NOT NULL,
  `bc_id` int(11) NOT NULL,
  `bcp_contact` varchar(20) NOT NULL,
  `bcp_status` enum('0','1','2','3') DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `border_control_personnel`
--

INSERT INTO `border_control_personnel` (`bcp_id`, `bc_id`, `bcp_contact`, `bcp_status`, `created`, `modified`) VALUES
(1, 1, '09305855792', '1', '2020-08-23 17:18:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `destination`
--

CREATE TABLE `destination` (
  `destination_id` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `from_street` varchar(50) DEFAULT NULL,
  `from_barangay` varchar(50) NOT NULL,
  `from_city` varchar(50) NOT NULL,
  `from_province` varchar(50) NOT NULL,
  `from_zip_code` varchar(50) DEFAULT NULL,
  `to_street` varchar(50) DEFAULT NULL,
  `to_barangay` varchar(50) NOT NULL,
  `to_city` varchar(50) NOT NULL,
  `to_province` varchar(50) NOT NULL,
  `to_zip_code` varchar(50) DEFAULT NULL,
  `status` enum('0','1') DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `destination`
--

INSERT INTO `destination` (`destination_id`, `vehicle_id`, `from_street`, `from_barangay`, `from_city`, `from_province`, `from_zip_code`, `to_street`, `to_barangay`, `to_city`, `to_province`, `to_zip_code`, `status`, `created`) VALUES
(1, 1, ' Paseo De Roxas St', 'Ayala', 'Makati City', 'Metro Manila', '1209', ' Deca Homes', 'Abella', 'Naga City', 'Camarines Sur', '4400', '0', '2020-08-16 15:38:28');

-- --------------------------------------------------------

--
-- Table structure for table `establishment`
--

CREATE TABLE `establishment` (
  `establishment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `establishment_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `permit_number` varchar(60) NOT NULL,
  `is_verified` enum('0','1','2') DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `establishment`
--

INSERT INTO `establishment` (`establishment_id`, `user_id`, `establishment_name`, `address`, `permit_number`, `is_verified`, `created`, `modified`) VALUES
(1, 1, 'Testing Establishment', 'Naga City', '20', '2', '2020-08-09 15:38:50', '2020-09-28 15:24:49'),
(2, 2, 'Testing Establishment', 'Naga City', '2021', '2', '2020-08-09 16:17:15', '2020-09-05 15:46:53'),
(3, 1, 'Testing Establishment', 'Naga City', '2021', '2', '2020-08-09 16:17:20', '2020-09-05 15:48:31'),
(4, 1, 'Testing Establishment', 'Naga City', '2021', '1', '2020-08-09 16:17:27', '2020-09-28 15:26:48'),
(5, 1, 'Testing Establishment Last', 'Naga City', '2021', '1', '2020-08-09 16:17:41', '2020-09-28 15:26:51');

-- --------------------------------------------------------

--
-- Table structure for table `generated_qrcode`
--

CREATE TABLE `generated_qrcode` (
  `generated_qrcode_id` int(11) NOT NULL,
  `qr_code` varchar(20) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `generated_qrcode`
--

INSERT INTO `generated_qrcode` (`generated_qrcode_id`, `qr_code`, `created`) VALUES
(1, 'ABC-9221217420', '2020-09-21 16:17:42'),
(2, 'ABC-9221217421', '2020-09-21 16:17:42'),
(3, 'ABC-9221217422', '2020-09-21 16:17:42'),
(4, 'ABC-9221217423', '2020-09-21 16:17:42'),
(5, 'ABC-9221217424', '2020-09-21 16:17:42'),
(6, 'ABC-9221217425', '2020-09-21 16:17:42'),
(7, 'ABC-9221217426', '2020-09-21 16:17:42'),
(8, 'ABC-9221217427', '2020-09-21 16:17:42'),
(9, 'ABC-9221217428', '2020-09-21 16:17:42'),
(10, 'ABC-9221217429', '2020-09-21 16:17:42'),
(11, 'ABC-922859270', '2020-09-22 00:59:27'),
(12, 'ABC-922859271', '2020-09-22 00:59:27'),
(13, 'ABC-922859272', '2020-09-22 00:59:27'),
(14, 'ABC-922859273', '2020-09-22 00:59:27'),
(15, 'ABC-922859274', '2020-09-22 00:59:27'),
(16, 'ABC-922859275', '2020-09-22 00:59:27'),
(17, 'ABC-922859276', '2020-09-22 00:59:27'),
(18, 'ABC-922859277', '2020-09-22 00:59:27'),
(19, 'ABC-922859278', '2020-09-22 00:59:27'),
(20, 'ABC-922859279', '2020-09-22 00:59:27'),
(21, 'ABC-9228592710', '2020-09-22 00:59:27'),
(22, 'ABC-9228592711', '2020-09-22 00:59:27'),
(23, 'ABC-9228592712', '2020-09-22 00:59:27'),
(24, 'ABC-9228592713', '2020-09-22 00:59:27'),
(25, 'ABC-9228592714', '2020-09-22 00:59:27'),
(26, 'ABC-9228592715', '2020-09-22 00:59:27'),
(27, 'ABC-9228592716', '2020-09-22 00:59:27'),
(28, 'ABC-9228592717', '2020-09-22 00:59:27'),
(29, 'ABC-9228592718', '2020-09-22 00:59:27'),
(30, 'ABC-9228592719', '2020-09-22 00:59:27'),
(31, 'ABC-9228592720', '2020-09-22 00:59:27'),
(32, 'ABC-9228592721', '2020-09-22 00:59:27'),
(33, 'ABC-9228592722', '2020-09-22 00:59:27'),
(34, 'ABC-9228592723', '2020-09-22 00:59:27'),
(35, 'ABC-9228592724', '2020-09-22 00:59:27'),
(36, 'ABC-9228592725', '2020-09-22 00:59:27'),
(37, 'ABC-9228592726', '2020-09-22 00:59:27'),
(38, 'ABC-9228592727', '2020-09-22 00:59:27'),
(39, 'ABC-9228592728', '2020-09-22 00:59:27'),
(40, 'ABC-9228592729', '2020-09-22 00:59:27'),
(41, 'ABC-9228592730', '2020-09-22 00:59:27'),
(42, 'ABC-9228592731', '2020-09-22 00:59:27'),
(43, 'ABC-9228592732', '2020-09-22 00:59:27'),
(44, 'ABC-9228592733', '2020-09-22 00:59:27'),
(45, 'ABC-9228592734', '2020-09-22 00:59:27'),
(46, 'ABC-9228592735', '2020-09-22 00:59:27'),
(47, 'ABC-9228592736', '2020-09-22 00:59:27'),
(48, 'ABC-9228592737', '2020-09-22 00:59:27'),
(49, 'ABC-9228592738', '2020-09-22 00:59:27'),
(50, 'ABC-9228592739', '2020-09-22 00:59:27'),
(51, 'ABC-9228592740', '2020-09-22 00:59:27'),
(52, 'ABC-9228592741', '2020-09-22 00:59:27'),
(53, 'ABC-9228592742', '2020-09-22 00:59:27'),
(54, 'ABC-9228592743', '2020-09-22 00:59:27'),
(55, 'ABC-9228592744', '2020-09-22 00:59:27'),
(56, 'ABC-9228592745', '2020-09-22 00:59:27'),
(57, 'ABC-9228592746', '2020-09-22 00:59:27'),
(58, 'ABC-9228592747', '2020-09-22 00:59:27'),
(59, 'ABC-9228592748', '2020-09-22 00:59:27'),
(60, 'ABC-9228592749', '2020-09-22 00:59:27'),
(61, 'ABC-9228592750', '2020-09-22 00:59:27'),
(62, 'ABC-9228592751', '2020-09-22 00:59:27'),
(63, 'ABC-9228592752', '2020-09-22 00:59:27'),
(64, 'ABC-9228592753', '2020-09-22 00:59:27'),
(65, 'ABC-9228592754', '2020-09-22 00:59:27'),
(66, 'ABC-9228592755', '2020-09-22 00:59:27'),
(67, 'ABC-9228592756', '2020-09-22 00:59:27'),
(68, 'ABC-9228592757', '2020-09-22 00:59:27'),
(69, 'ABC-9228592758', '2020-09-22 00:59:27'),
(70, 'ABC-9228592759', '2020-09-22 00:59:27'),
(71, 'ABC-9228592760', '2020-09-22 00:59:27'),
(72, 'ABC-9228592761', '2020-09-22 00:59:27'),
(73, 'ABC-9228592762', '2020-09-22 00:59:27'),
(74, 'ABC-9228592763', '2020-09-22 00:59:27'),
(75, 'ABC-9228592764', '2020-09-22 00:59:27'),
(76, 'ABC-9228592765', '2020-09-22 00:59:27'),
(77, 'ABC-9228592766', '2020-09-22 00:59:27'),
(78, 'ABC-9228592767', '2020-09-22 00:59:27'),
(79, 'ABC-9228592768', '2020-09-22 00:59:27'),
(80, 'ABC-9228592769', '2020-09-22 00:59:27'),
(81, 'ABC-9228592770', '2020-09-22 00:59:27'),
(82, 'ABC-9228592771', '2020-09-22 00:59:27'),
(83, 'ABC-9228592772', '2020-09-22 00:59:27'),
(84, 'ABC-9228592773', '2020-09-22 00:59:27'),
(85, 'ABC-9228592774', '2020-09-22 00:59:27'),
(86, 'ABC-9228592775', '2020-09-22 00:59:27'),
(87, 'ABC-9228592776', '2020-09-22 00:59:27'),
(88, 'ABC-9228592777', '2020-09-22 00:59:27'),
(89, 'ABC-9228592778', '2020-09-22 00:59:27'),
(90, 'ABC-9228592779', '2020-09-22 00:59:27'),
(91, 'ABC-9228592780', '2020-09-22 00:59:27'),
(92, 'ABC-9228592781', '2020-09-22 00:59:27'),
(93, 'ABC-9228592782', '2020-09-22 00:59:27'),
(94, 'ABC-9228592783', '2020-09-22 00:59:27'),
(95, 'ABC-9228592784', '2020-09-22 00:59:27'),
(96, 'ABC-9228592785', '2020-09-22 00:59:27'),
(97, 'ABC-9228592786', '2020-09-22 00:59:27'),
(98, 'ABC-9228592787', '2020-09-22 00:59:27'),
(99, 'ABC-9228592788', '2020-09-22 00:59:27'),
(100, 'ABC-9228592789', '2020-09-22 00:59:27'),
(101, 'ABC-9228592790', '2020-09-22 00:59:27'),
(102, 'ABC-9228592791', '2020-09-22 00:59:27'),
(103, 'ABC-9228592792', '2020-09-22 00:59:27'),
(104, 'ABC-9228592793', '2020-09-22 00:59:27'),
(105, 'ABC-9228592794', '2020-09-22 00:59:27'),
(106, 'ABC-9228592795', '2020-09-22 00:59:27'),
(107, 'ABC-9228592796', '2020-09-22 00:59:27'),
(108, 'ABC-9228592797', '2020-09-22 00:59:27'),
(109, 'ABC-9228592798', '2020-09-22 00:59:27'),
(110, 'ABC-9228592799', '2020-09-22 00:59:27');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `log_id` int(11) NOT NULL,
  `establishment_id` int(11) NOT NULL,
  `scanner_user_id` int(11) NOT NULL,
  `public_user_id` int(11) NOT NULL,
  `temperature` double NOT NULL,
  `sign_in_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `sign_out_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`log_id`, `establishment_id`, `scanner_user_id`, `public_user_id`, `temperature`, `sign_in_date`, `sign_out_date`) VALUES
(1, 1, 1, 2, 37.2, '2020-09-05 17:20:35', NULL),
(2, 1, 1, 2, 38.2, '2020-09-05 17:20:36', NULL),
(3, 1, 1, 2, 37.9, '2020-09-05 17:59:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `passenger`
--

CREATE TABLE `passenger` (
  `passenger_id` int(11) NOT NULL,
  `destination_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `temperature` varchar(20) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `passenger`
--

INSERT INTO `passenger` (`passenger_id`, `destination_id`, `user_id`, `temperature`, `created`, `modified`) VALUES
(1, 1, 1, '35.6', '2020-08-16 16:58:08', NULL),
(2, 1, 2, '37.6', '2020-08-16 16:58:22', NULL),
(3, 1, 3, '32.6', '2020-08-16 16:58:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `personnel`
--

CREATE TABLE `personnel` (
  `personnel_id` int(11) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `establishment_id` int(11) NOT NULL,
  `personnel_status` enum('0','1','2','3') DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `personnel`
--

INSERT INTO `personnel` (`personnel_id`, `contact`, `establishment_id`, `personnel_status`, `created`, `modified`) VALUES
(1, '09305855792', 1, '1', '2020-08-09 16:11:12', '2020-09-05 17:21:15');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_qrcode` varchar(20) DEFAULT NULL,
  `user_type` enum('0','1','2','3','4') DEFAULT '0',
  `firstname` varchar(60) NOT NULL,
  `middlename` varchar(60) DEFAULT NULL,
  `lastname` varchar(60) NOT NULL,
  `age` smallint(6) NOT NULL,
  `gender` enum('M','F') NOT NULL,
  `contact` varchar(20) NOT NULL,
  `password` varchar(120) NOT NULL,
  `conf_number` int(11) NOT NULL,
  `user_img` varchar(150) DEFAULT NULL,
  `street` varchar(60) NOT NULL,
  `barangay` varchar(60) NOT NULL,
  `city` varchar(60) NOT NULL,
  `province` varchar(60) NOT NULL,
  `zip_code` varchar(10) NOT NULL,
  `status` enum('0','1') DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_qrcode`, `user_type`, `firstname`, `middlename`, `lastname`, `age`, `gender`, `contact`, `password`, `conf_number`, `user_img`, `street`, `barangay`, `city`, `province`, `zip_code`, `status`, `created`, `modified`) VALUES
(1, 'qr-001', '4', 'Azure', 'B', 'Kite', 20, 'M', '09305855791', '$2y$10$DeuyaVwlHt/BKh4WiUSWHurYM9hXvnvUgAHIbLy4TwWcIEozKZeue', 689500, NULL, 'Bayawas', 'Abella', 'Naga', 'Camarines Sur', '4426', '1', '2020-08-02 15:59:12', '2020-08-16 16:48:48'),
(2, 'qr-002', '0', 'Vergel', 'B', 'Basco', 20, 'M', '09305855792', '$2y$10$wa7JQU/SAnlwFYkkhphsEeUTB8KF.p2srTehZ4gQkv6sPpeJzE2ya', 264478, NULL, 'Bayawas', 'Abella', 'Naga', 'Camarines Sur', '4426', '1', '2020-08-09 16:05:09', '2020-08-16 16:49:37'),
(3, NULL, '4', 'Paulo', 'B', 'Enciso', 20, 'M', '09305855793', '$2y$10$LQwIwdojcFr5NcbzYfM8QOv8on6EMrEh0eK6eFRfp478EbIDYNrgy', 906529, NULL, 'Bayawas', 'Abella', 'Naga', 'Camarines Sur', '4426', '1', '2020-08-10 15:35:06', '2020-08-16 16:49:27'),
(4, 'qqwqw', '0', 'qwqw', 'qwqw', 'qwqw', 12, 'M', '09305855797', '$2y$10$XvWQYu02MdIq45cOfUR3xexmxMB.DLBeY4ouJ6nV3K2tGANimpG7y', 289913, NULL, 'ad', 'ada', 'dad', 'asas', 'asas', '0', '2020-09-21 15:38:46', NULL),
(5, 'SYS-921552141', '0', 'xxx', 'xxx', 'xxx', 11, 'M', '09305855799', '$2y$10$McWo1oEaGfFCULLC97lr7.F6mDnl3ad29JNN7z80xspubB4/SBmoW', 720364, NULL, 'ad', 'ada', 'dad', 'asas', 'asas', '0', '2020-09-21 15:52:14', NULL),
(6, 'SYS-921632248', '0', 'qwqw', 'qwqw', 'qwqw', 11, 'M', '09305855780', '$2y$10$Jw9qy7NizGUqSJ75GgiegefWYOw451SfKF5zz6x/WeIPYjeNhjZTy', 155193, NULL, 'ad', 'ada', 'dad', 'asas', 'asas', '0', '2020-09-21 16:32:24', NULL),
(7, 'SYS-921637062', '0', 'qwqw', 'qwqw', 'qwqw', 12, 'M', '09305855693', '$2y$10$oHP51mkIMnEzvtkKsL3n3upJxm3W3HUFLYbafnw.dvnGL7vtAH72O', 125609, NULL, 'ad', 'ada', 'dad', 'asas', 'asas', '0', '2020-09-21 16:37:06', NULL),
(8, 'ABC-9221217420', '0', 'qwqw', 'qwqw', 'qwqw', 12, 'M', '09305855767', '$2y$10$VWSYhuH6xKP9xw0HoWjUAuKanm98dTcjxaIampRQ9qZ3.zMLwDnJ2', 823607, NULL, 'ad', 'ada', 'dad', 'asas', 'asas', '0', '2020-09-21 16:38:06', NULL),
(9, 'ABC-9221217426', '0', 'Vergel', 'B', 'Basco', 22, 'M', '09123456782', '$2y$10$IBwxPG9Mw9fxbWedk3CyBuIV5zysS54BGc0lKqLi78qLwcIbv3ZmC', 226837, NULL, 'Deca', 'Abella', 'Naga', 'Camarines Sur', '4400', '1', '2020-09-22 00:58:44', '2020-09-28 12:11:25');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle`
--

CREATE TABLE `vehicle` (
  `vehicle_id` int(11) NOT NULL,
  `vehicle_type` enum('0','1','2','3','4','5','6','7') NOT NULL,
  `vehicle_qrcode` varchar(20) DEFAULT NULL,
  `brand` varchar(50) NOT NULL,
  `color` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `plate_num` varchar(20) NOT NULL,
  `is_public` enum('0','1') DEFAULT '0',
  `vehicle_img` varchar(150) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle`
--

INSERT INTO `vehicle` (`vehicle_id`, `vehicle_type`, `vehicle_qrcode`, `brand`, `color`, `model`, `plate_num`, `is_public`, `vehicle_img`, `created`, `modified`) VALUES
(1, '', 'vec-101,', 'Yamaha,', 'Black,', 'Version 2', 'sample-101', '1', '', '2020-08-16 15:10:10', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `border_control`
--
ALTER TABLE `border_control`
  ADD PRIMARY KEY (`bc_id`);

--
-- Indexes for table `border_control_personnel`
--
ALTER TABLE `border_control_personnel`
  ADD PRIMARY KEY (`bcp_id`),
  ADD KEY `bc_id` (`bc_id`),
  ADD KEY `bcp_contact` (`bcp_contact`);

--
-- Indexes for table `destination`
--
ALTER TABLE `destination`
  ADD PRIMARY KEY (`destination_id`),
  ADD KEY `vehicle_id` (`vehicle_id`);

--
-- Indexes for table `establishment`
--
ALTER TABLE `establishment`
  ADD PRIMARY KEY (`establishment_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `generated_qrcode`
--
ALTER TABLE `generated_qrcode`
  ADD PRIMARY KEY (`generated_qrcode_id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `establishment_id` (`establishment_id`),
  ADD KEY `scanner_user_id` (`scanner_user_id`),
  ADD KEY `public_user_id` (`public_user_id`);

--
-- Indexes for table `passenger`
--
ALTER TABLE `passenger`
  ADD PRIMARY KEY (`passenger_id`),
  ADD KEY `destination_id` (`destination_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `personnel`
--
ALTER TABLE `personnel`
  ADD PRIMARY KEY (`personnel_id`),
  ADD KEY `contact` (`contact`),
  ADD KEY `establishment_id` (`establishment_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `contact` (`contact`);

--
-- Indexes for table `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`vehicle_id`),
  ADD UNIQUE KEY `vehicle_qrcode` (`vehicle_qrcode`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `border_control`
--
ALTER TABLE `border_control`
  MODIFY `bc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `border_control_personnel`
--
ALTER TABLE `border_control_personnel`
  MODIFY `bcp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `destination`
--
ALTER TABLE `destination`
  MODIFY `destination_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `establishment`
--
ALTER TABLE `establishment`
  MODIFY `establishment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `generated_qrcode`
--
ALTER TABLE `generated_qrcode`
  MODIFY `generated_qrcode_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `passenger`
--
ALTER TABLE `passenger`
  MODIFY `passenger_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `personnel`
--
ALTER TABLE `personnel`
  MODIFY `personnel_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `vehicle`
--
ALTER TABLE `vehicle`
  MODIFY `vehicle_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `border_control_personnel`
--
ALTER TABLE `border_control_personnel`
  ADD CONSTRAINT `border_control_personnel_ibfk_1` FOREIGN KEY (`bc_id`) REFERENCES `border_control` (`bc_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `border_control_personnel_ibfk_2` FOREIGN KEY (`bcp_contact`) REFERENCES `users` (`contact`) ON DELETE CASCADE;

--
-- Constraints for table `destination`
--
ALTER TABLE `destination`
  ADD CONSTRAINT `destination_ibfk_1` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicle` (`vehicle_id`) ON DELETE CASCADE;

--
-- Constraints for table `establishment`
--
ALTER TABLE `establishment`
  ADD CONSTRAINT `establishment_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE;

--
-- Constraints for table `log`
--
ALTER TABLE `log`
  ADD CONSTRAINT `log_ibfk_1` FOREIGN KEY (`establishment_id`) REFERENCES `establishment` (`establishment_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `log_ibfk_2` FOREIGN KEY (`scanner_user_id`) REFERENCES `personnel` (`personnel_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `log_ibfk_3` FOREIGN KEY (`public_user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE;

--
-- Constraints for table `passenger`
--
ALTER TABLE `passenger`
  ADD CONSTRAINT `passenger_ibfk_1` FOREIGN KEY (`destination_id`) REFERENCES `destination` (`destination_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `passenger_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE;

--
-- Constraints for table `personnel`
--
ALTER TABLE `personnel`
  ADD CONSTRAINT `personnel_ibfk_1` FOREIGN KEY (`contact`) REFERENCES `users` (`contact`) ON DELETE CASCADE,
  ADD CONSTRAINT `personnel_ibfk_2` FOREIGN KEY (`establishment_id`) REFERENCES `establishment` (`establishment_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
